<?php 
$allow = array(1 => true, 2 => true, 3 => true);

$perfiles = array(1 => "Administrador", "Supervisor", "Operador");

include('session.inc');
include('utils.inc');

$tipo_form = $_GET["tipo"];

if ($_POST["commit"] && $_POST["type"] != NULL)
  $tipo_form = $_POST["type"];

if ($tipo_form == NULL) die('tipo de formulario no especificado');

$con = NULL;

switch ($tipo_form)
  {
  case 1:
  case 2:
  case 3:
  case 4:
    $con = make_connection();
    break;
  default:
    die("Tipo de formulario inv&aacute;lido");
  }

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    $tiempo_care = trim($_POST['tiempo_care']);
    
    if (!ereg('^[0-5]?[0-9]:[0-5][0-9]$', $tiempo_care))
     // $errors[] = "Tiempo CARE";

    switch ($tipo_form)
      {
      case 1:
	if ((1 > strlen(trim($_POST['cuenta'])))
	    || !ereg('^[A-Z]{2}-[0-9]{1,7}$',
		     strtoupper(trim($_POST['cuenta']))))
	  $errors[] = "ID de alarma";
	$capcode = trim($_POST['cap_code']);
	if (7 != strlen($capcode) || !is_numeric($capcode))
	  $errors[] = "Cap Code";
	break;
      case 2:
      case 3:
	if (1 > strlen(trim($_POST['direccion'])))
	  $errors[] = "Direcci&oacute;n";
	if ("2" == $tipo_form)
	  {
	    $tel = ereg_replace("[ -]", "", trim($_POST['telefono']));
	    if (8 > strlen($tel) || !is_numeric($tel))
	      $errors[] = "Tel&eacute;fono";
	  }
	if (1 > strlen(trim($_POST['colonia'])))
	  $errors[] = "Colonia";
	break;
      }

    $valid = count($errors) == 0;

    if ($valid)
      {
	$vars = "medio_de_recepcion_id, tipo_de_suceso_id, canalizacion_id, " .
	  "proteccion_civil, seguridad_publica, turno, tiempo_care, " .
	  "tiempo_base_plata, notas_abre, suceso_relevante, " . 
	  "tipo_de_registro_id, abierto, usuario_crea, perfil_usuario_crea, " .
	  "usuario_cierra, perfil_usuario_cierra, notas_cierra";
	
	$time = localtime();
	$minutes = $time['tm_min'] + $time['tm_hour'] * 60;
	$turn = 0;

	if ($minutes >= 6*60 || $minutes <= 14*60) $turn = 0;
	else if ($minutes >= 14*60 + 1 || $minutes <= 22*60) $turn = 1;
	else $turn = 2;
	
	$values = $tipo_form . ", " . $_POST['suceso'] . ", " .
	  $_POST['canalizacion'] . ", " .
	  sqlquote($_POST['proteccion_civil']) . ", " .
	  sqlquote($_POST['seguridad_publica']) . ", " .
	  $turn . ", " .
	  sqlquote('00:' . $tiempo_care) . ", 0, " .
	  sqlquote($_POST['notas']) . ", " .
	  ($_POST['suceso_relevante'] ? "true" : "false") . ", " .
	  $_POST['tipo_registro'] . ", " . $_POST['estado'] . ", " .
	  sqlquote($_SESSION['login_name']) . ", "  .
	  sqlquote($perfiles[$_SESSION['profile']]) . ", " .
	  ($_POST['estado'] == "0" ? sqlquote($_SESSION['login_name']) : sqlquote("")) . ", " .
	  ($_POST['estado'] == "0" ? sqlquote($perfiles[$_SESSION['profile']]) : sqlquote("")) . ", " .
	  sqlquote("");
	
	$stable = NULL;
	$svars = NULL;
	$svalues = NULL;
	
	switch ($tipo_form)
	  {
	  case 1:
	    $stable = "eventos_alarmas";
	    $svars = "cuenta, cap_code";
	    $svalues = sqlquote(strtoupper($_POST['cuenta'])) . ", " .
	      sqlquote($capcode);
	    break;
	  case 2:
	  case 3:
	    $colonias = get_poplist_table_array('colonias');
	    $colonia = $colonias[$_POST['colonia']];
	    $sector = mysql_fetch_row(mysql_query('SELECT sector_id FROM colonias WHERE id = ' . $_POST['colonia'] . ';'));
	    $tel = ereg_replace("[ -]", "", trim($_POST['telefono']));
	    $stable = "eventos_radio_telefono";
	    $svars = "direccion, colonia, sector_id, telefono, calles";
	    $svalues = sqlquote($_POST['direccion']) . ", " .
	      sqlquote($colonia) . ", " .
	      $sector[0] . ", " .
	      sqlquote($tel) . ", " . 
	      sqlquote($_POST['calles']);
	    break;
	  case 4:
	    $stable = "eventos_monitores";
	    $svars = "camara_id";
	    $svalues = $_POST['camara'];
	    break;
	  }
	
	$query = "INSERT INTO sucesos (fecha, hora, $vars) " .
	  "values(CURDATE(), CURTIME(), $values);";
	mysql_query($query);
	$id = mysql_insert_id();
	$squery = "INSERT INTO $stable (id, $svars) values($id, $svalues);";
	mysql_query($squery);
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=sucesos.php" />';
  }

include('header.html');

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="sucesos.php">Sucesos</a>: 
      </span>
      <span id="title_center"><?php
      $titulos = array("", "Alarma", "Captura de Sucesos", "Radio", "Monitor");
        echo $titulos[$tipo_form];
      ?></span>
    </div>
    <form action="form_suceso.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>El contenido de los siguientes campos no es v&aacute;lido:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
      <input type="hidden" style="display: none;" name="commit" value="1" />
      <input type="hidden" style="display: none;" name="type"
             <?php echo 'value="' . $tipo_form . '"'; ?> />
      <label class="frm" for="fecha">Fecha:</label>
      <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
	     value=<? echo '"' . date("d/m/Y H:i") . '"'; ?> />
      <br />
      <?php
  switch ($tipo_form)
    {
    case 1: ?>
      <label class="frm" for="cuenta">ID de alarma:</label>
      <?php make_input('cuenta'); ?>
      <label class="frm" for="cap_code">Cap Code:</label>
      <?php make_input('cap_code'); ?>
      <br />
      <?php
      break;
    case 2:
    case 3:?>
      <label class="frm" for="direccion">Direcci&oacute;n:</label>
      <?php make_input('direccion'); ?>
      <label class="frm" for="calles">Entre las calles:</label>
      <?php make_input('calles'); ?>
      <br />
      <label class="frm" for="colonia">Colonia:</label>
      <?php make_select('colonia', 'colonias'); ?>
      <?php
	 if ($tipo_form == "2")
	   {
	     echo '<label class="frm" for="telefono">Tel&eacute;fono:</label>' . "\n";
	     make_input('telefono');
	   }
         echo '<br />' . "\n";
      break;
    case 4: ?>
      <label class="frm" for="camara">C&aacute;mara:</label>
      <?php make_select('camara', 'camaras'); ?>
      <br />
      <?php
      break;
    } ?>
      <label class="frm" for="suceso">Suceso:</label>
      <?php make_select('suceso', 'tipos_de_suceso'); ?>
      <br />
      <label class="frm" for="canalizacion">Canalizaci&oacute;n:</label>
      <?php make_select('canalizacion', 'canalizaciones'); ?>
      <label class="frm" for="tiempo_care">Tiempo de respuesta (MM:SS):</label>
      <?php make_input('tiempo_care'); ?>
      <br />
     <!-- <label class="frm" for="proteccion_civil">Protecci&oacute;n Civil:</label>
      <?php //make_input('proteccion_civil'); ?>-->
     <!-- <label class="frm" for="seguridad_publica">Seguridad P&uacute;blica:</label>
      <?php //make_input('seguridad_publica'); ?>-->
      <br />
      <label class="frm" for="suceso_relevante">Suceso relevante:</label>
      <input class="cb" type="checkbox" id="suceso_relevante"
             name="suceso_relevante" value="1"
             <?php if ($_POST['suceso_relevante'] == "1")
                     echo 'checked="true"'; ?> />
      <br />
      <label class="frm" for="notas">Notas:</label>
      <textarea class="frm" id="notas" name="notas"
	><?php echo uhtmlentities($_POST['notas']); ?></textarea>
      <br />
      <label class="frm" for="tipo_registro">Tipo de registro:</label>
      <select class="frm" id="tipo_registro" name="tipo_registro">
	<?php
	   make_option("tipo_registro", "Ver&iacute;dico", "0");
	   make_option("tipo_registro", "Accidental", "1"); 
	?>
      </select>
      <label class="frm" for="estado">Estado:</label>
      <select class="frm" id="estado" name="estado">
	<?php
	   make_option("estado", "Abierto", "1");
	   make_option("estado", "Cerrado", "0"); 
	?>
      </select><br />
      <input id="enviar" name="enviar" type="submit" value="Enviar" />
      <br />
      </div>
    </form>


<?php
  }

include("footer.html");

end_connection($con);
?>
<script>
a = setTimeout("alert('Se�or Operador cierre su facebook y pongase a trabajar!')",100000);
</script>
