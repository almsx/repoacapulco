<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$id = NULL;
$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    if ((1 > strlen(trim($_POST['destinatario']))))
      $errors[] = "Destinatario";
    if ((1 > strlen(trim($_POST['cargo_destinatario']))))
      $errors[] = "Cargo del destinatario";
    if ((1 > strlen(trim($_POST['remitente']))))
      $errors[] = "Remitente";
    if ((1 > strlen(trim($_POST['cargo_remitente']))))
      $errors[] = "Cargo del remitente";
    if ((1 > strlen(trim($_POST['texto']))))
      $errors[] = "Texto";
    //    if (0 < strlen($_POST['folio'])
    //	&& (6 > strlen($_POST['folio']) || 12 < strlen($_POST['folio'])))
    //      $errors[] = "Folio";
    
    $valid = count($errors) == 0;

    if ($valid)
      {
	$vars = 'destinatario, destinatario_cargo, remitente, remitente_cargo, texto, destinatario_de_copia, folio';
	
	$values = sqlquote(ucwords(strtolower($_POST['destinatario']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['cargo_destinatario']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['remitente']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['cargo_remitente']))) . ", " .
	  sqlquote($_POST['texto']) . ", " .
	  sqlquote(ucwords(strtolower($_POST['destinatario_copia']))) . ", " .
	  sqlquote("");
	
	$query = "INSERT INTO tarjetas_informativas (fecha, $vars) " .
	  "values(CURDATE(), $values);";

	mysql_query($query);
	$id = mysql_insert_id();
	$year = strftime("%Y");
	$folio = sprintf("CARE/%04d/$year", $id);
	mysql_query('UPDATE tarjetas_informativas SET folio = "' . $folio . '" WHERE id = ' . $id . ";");
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";
$opendoc = NULL;

if ($redirect)
  $opendoc = "ver_tarjeta.php?id=$id";

include('header.html');

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
    <p>
      <a class="button_link" target="_blank"
	 href=<?php echo "\"$opendoc\""; ?> >Ver documento</a>
    </p>
    <p>
      <a class="button_link"
	 href="tarjetas_informativas.php">Volver</a>
    </p>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="tarjetas_informativas.php">Tarjetas informativas</a>: 
      </span>
      <span id="title_center">Crear tarjeta informativa</span>
    </div>
    <form action="crear_tarjeta.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>El contenido de los siguientes campos no es v&aacute;lido:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
      <input type="hidden" style="display: none;" name="commit" value="1" />
      <label class="frm" for="fecha">Fecha:</label>
      <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
	     value=<? echo '"' . date("d/m/Y H:i") . '"'; ?> />
      <br />
      <label class="frm" for="destinatario">Destinatario:</label>
      <?php make_input('destinatario'); ?>
      <label class="frm" for="cargo_destinatario">Cargo del destinatario:</label>
      <?php make_input('cargo_destinatario'); ?>
      <br />
      <label class="frm" for="remitente">Remitente:</label>
      <?php make_input('remitente'); ?>
      <label class="frm" for="cargo_remitente">Cargo del remitente:</label>
      <?php make_input('cargo_remitente'); ?>
      <br />
      <label class="frm" for="destinatario_copia">Destinatarios de copia:</label>
      <?php make_input('destinatario_copia'); ?>
      <label class="frm">(ej: Jos&eacute; L&oacute;pez Director CARE, Julio Romero Supervisor)</label>
      <br />
      <label class="frm" for="texto">Texto:</label>
      <textarea class="frm" id="texto" name="texto"
	><?php echo uhtmlentities($_POST['texto']); ?></textarea>
      <br />
      <input id="enviar" name="enviar" type="submit" value="Enviar" />
      <br />
      </div>
    </form>

<?php
  }

include("footer.html");

end_connection($con);
?>
