<?php
$allow = array(1 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    $exists = mysql_fetch_row(mysql_query('SELECT id FROM usuarios WHERE nombre = "' . $_POST['username'] . '";'));

    if ($exists)
      $errors[] = "Ya existe un usuario con ese nombre";
    if (1 > strlen($_POST['username'])
	|| trim($_POST['username']) != $_POST['username'] )
      $errors[] = "Nombre de usuario inv&aacute;lido";
    if (ereg('^$', $_POST['password']))
      $errors[] = "Contrase&ntilde;a inv&aacute;lida";
    if ($_POST['password'] != $_POST['password_again'])
      $errors[] = "Las contrase&ntilde;as no coinciden";
    if (1 > strlen($_POST['nombre']))
      $errors[] = "Nombre";
    if (1 > strlen($_POST['apellidos']))
      $errors[] = "Apellidos";
    $tel = ereg_replace("[ -]", "", trim($_POST['telefono']));
    if (8 > strlen($tel) || !is_numeric($tel))
      $errors[] = "Tel&eacute;fono";
    if (1 > strlen($_POST['direccion']))
      $errors[] = "Direcci&oacute;n";
    if (1 > strlen($_POST['dependencia']))
      $errors[] = "Dependencia";

    $valid = (count($errors) == 0);

    if ($valid)
      {
	$vars = 'nombre, pass, perfil, nombre_persona, apellidos, telefono, direccion, dependencia';
	
	$values = sqlquote($_POST['username']) . ", " .
	  'MD5("' . mysql_real_escape_string($_POST['password']) . '"), ' .
	  $_POST['perfil'] . ", " .
	  sqlquote(ucwords(strtolower($_POST['nombre']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['apellidos']))) . ", " .
	  sqlquote($tel) . ", " .
	  sqlquote($_POST['direccion']) . ", " .
	  sqlquote($_POST['dependencia']);
	
	$query = "INSERT INTO usuarios ($vars) values($values);";

	mysql_query($query);
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=admin_usuarios.php" />';
  }

include("header.html");

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="admin.php">Administrador</a>: 
        <a class="title_link" href="admin_usuarios.php">Usuarios</a>: 
      </span>
      <span id="title_center">Nuevo</span>
    </div>
    <form action="nuevo_usuario.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>No se pudo completar la acci&oacute;n debido a los siguiente errores:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
        <input type="hidden" style="display: none;" name="commit" value="1" />
	<label class="frm" for="username">Nombre de usuario:</label>
	<?php make_input('username'); ?>
	<label class="frm" for="password">Contrase&ntilde;a:</label>
	<input class="frm" type="password" id="password" name="password" value="" />
	<br />
	<label class="frm" for="perfil">Perfil:</label>
	<select class="frm" id="perfil" name="perfil">
	<?php
	   make_option("perfil", "Administrador", "1");
	   make_option("perfil", "Supervisor", "2"); 
	   make_option("perfil", "Operador", "3"); 
	   make_option("perfil", "Visitante", "4"); 
	?>
	</select>
        <label class="frm" for="password">Contrase&ntilde;a (repetir):</label>
	<input class="frm" type="password" id="password_again" name="password_again" value="" />
	<br />
	<label class="frm" for="nombre">Nombre:</label>
	<?php make_input('nombre'); ?>
	<label class="frm" for="apellidos">Apellidos:</label>
	<?php make_input('apellidos'); ?>
	<br />
	<label class="frm" for="telefono">Tel&eacute;fono:</label>
	<?php make_input('telefono'); ?>
        <label class="frm" for="direccion">Direcci&oacute;n:</label>
	<?php make_input('direccion'); ?>
	<br />
	<label class="frm" for="dependencia">Dependencia:</label>
	<?php make_input('dependencia'); ?>
	<br />
	<br />
	<br />
	<input id="crear" name="crear" type="submit" value="Crear" />
	<br />
      </div>
    </form>

<?php
  }
include("footer.html");
?>
