<?php
$allow = array(1 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    $exists = mysql_fetch_row(mysql_query('SELECT id FROM usuarios WHERE nombre = "' . $_POST['username'] . '";'));

    if (!$exists)
      $errors[] = "El usuario no existe";
    if ($_POST['username'] == $_SESSION['login_name'])
      $errors[] = "No se puede borrar al usuario actual";

    $valid = (count($errors) == 0);

    if ($valid)
      {
	$username = sqlquote($_POST['username']);
	$query = "DELETE FROM usuarios WHERE nombre = $username;";

	mysql_query($query);
      }
  }

$redirect = $_POST['borrar'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=admin.php" />';
  }

include("header.html");

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="admin.php">Administrador</a>: 
        <a class="title_link" href="admin_usuarios.php">Usuarios</a>: 
      </span>
      <span id="title_center">Borrar</span>
    </div>
    <form action="borrar_usuario.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>No se pudo completar la acci&oacute;n debido a los siguientes errores:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
        <input type="hidden" style="display: none;" name="commit" value="1" />
	<label class="frm" for="username">Nombre de usuario:</label>
	<select class="frm" id="username" name="username">
	<?php make_usersoptions('username'); ?>
        </select>
	<br />
	<br />
	<br />
	<input id="borrar" name="borrar" type="submit" value="Borrar" />
	<br />
      </div>
    </form>

<?php
  }
include("footer.html");
end_connection($con);
?>
