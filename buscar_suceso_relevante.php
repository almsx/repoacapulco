<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

if (!$_GET['pdf'])
  include('session.inc');
include("utils.inc");

$con = make_connection();

$results = NULL;

if ($_GET['buscar'])
  {
    $filters = array();
    $filters_alarm = array();
    $filters_radio = array();
    $filters_monitor = array();
    $filter_string = "";
    $query = NULL;

    $hora_inicio = $_GET['hora_inicio_h'] . ':' . $_GET['hora_inicio_m'] . ':00';
    $hora_fin = $_GET['hora_fin_h'] . ':' . $_GET['hora_fin_m'] . ':59';

    if ($_GET['hora_inicio'])
      $filters[] = 'hora >= ' . sqlquote($hora_inicio);
    if ($_GET['hora_fin'])
      $filters[] = 'hora <= ' . sqlquote($hora_fin);
    if ($_GET['fecha_inicio'] && $_GET['fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha >= ' . sqlquote($_GET['fecha_inicio']);
    if ($_GET['fecha_fin'] && $_GET['fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha <= ' . sqlquote($_GET['fecha_fin']);
    if ($_GET['suceso'] && $_GET['suceso'] != "-1")
      $filters[] = 'tipo_de_suceso_id = ' . $_GET['suceso'];
    if ($_GET['tipo_registro'] && $_GET['tipo_registro'] != "-1")
      $filters[] = 'tipo_de_registro_id = ' . $_GET['tipo_registro'];
    if ($_GET['estado'] && $_GET['estado'] != "-1")
      $filters[] = 'abierto = ' . $_GET['estado'];
    if ($_GET['canalizacion'] && $_GET['canalizacion'] != "-1")
      $filters[] = 'canalizacion_id = ' . $_GET['canalizacion'];
    if ($_GET['turno'] && $_GET['turno'] != "-1")
      $filters[] = 'turno = ' . $_GET['turno'];
    if ($_GET['recepcion'] && $_GET['recepcion'] != "-1")
      $filters[] = 'medio_de_recepcion_id = ' . $_GET['recepcion'];
    if (0 < strlen(trim($_GET['usuario_crea'])))
      $filters[] = 'usuario_crea LIKE ' . sqlquote('%' . ucwords(strtolower($_GET['usuario_crea'])) . '%');
    if (0 < strlen(trim($_GET['usuario_cierra'])))
      $filters[] = 'usuario_cierra LIKE ' . sqlquote('%' . ucwords(strtolower($_GET['usuario_cierra'])) . '%');
    if (0 < strlen(trim($_GET['cuenta'])))
      $filters_alarm[] = 'cuenta = ' . sqlquote(strtoupper($_GET['cuenta']));

    if (0 < strlen(trim($_GET['colonia'])))
      $filters_radio[] = 'colonia = ' . sqlquote(wcwordss(strtolower($_GET['colonia'])));
    if ($_GET['sector'] && $_GET['sector'] != "-1")
      $filters_radio[] = 'sector_id = ' . $_GET['sector'];

    $tel = ereg_replace("[ -]", "", trim($_GET['telefono']));
    if (0 < strlen($tel))
      $filters_radio[] = 'telefono = ' . sqlquote($tel);

    if ($_GET['camara'] && $_GET['camara'] != "-1")
      $filters_monitor[] = 'camara_id = ' . $_GET['camara'];

    if (!((count($filters_alarm) > 0 && count($filters_radio) > 0)
	  || (count($filters_alarm) > 0 && count($filters_monitor) > 0)
	  || (count($filters_monitor) > 0 && count($filters_radio) > 0)))
      {
	$query = "SELECT s.id, s.fecha, s.hora, s.medio_de_recepcion_id, s.tipo_de_suceso_id, s.abierto, s.notas_abre, s.notas_cierra FROM sucesos AS s";	

	if (count($filters) > 0)
	  $filterstr .= "s." . implode(" AND s.", $filters);

	if (count($filters_alarm) > 0)
	  {
	    $filterstr .= (strlen($filterstr) > 0 ? " AND " : "") . "a." .
	      implode(" AND a.", $filters_alarm);
	    $query .= " INNER JOIN eventos_alarmas AS a ON s.id = a.id ";
	  }

	if (count($filters_radio) > 0)
	  {
	    $filterstr .= (strlen($filterstr) > 0 ? " AND " : "") . "r." .
	      implode(" AND r.", $filters_radio);
	    $query .= " INNER JOIN eventos_radio_telefono AS r ON s.id = r.id ";
	  }

	if (count($filters_monitor) > 0)
	  {
	    $filterstr .= (strlen($filterstr) > 0 ? " AND " : "") . "m." .
	      implode(" AND m.", $filters_monitor);
	    $query .= " INNER JOIN eventos_monitores AS m ON s.id = m.id ";
	  }
	
	$query .= (strlen($filterstr) > 0 ? " WHERE suceso_relevante = 1 AND $filterstr;" : ' WHERE suceso_relevante = 1;');
        $results = mysql_query($query);
      }
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");

?>
    <?php if (!$_GET['pdf']) { ?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="reportes.php">Reportes</a>:
      </span>
      <span id="title_center">B&uacute;squeda de sucesos relevantes</span>
    </div>
    <form action="buscar_suceso_relevante.php" method="GET">
      <div id="form">
      <label class="frm" for="fecha_inicio">Fecha inicial:</label>
      <script>DateInput('fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="fecha_fin">Fecha final:</label>
      <script>DateInput('fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="hora_inicio_h">Horario (HH:MM):</label>
      <label class="frm" style="width: auto; margin-left: 20px; " for="hora_inicio_h">desde:</label>
      <input class="cb" type="checkbox" id="hora_inicio" name="hora_inicio"
             value="1"
	     <?php if ($_GET['hora_inicio'] == "1")
                     echo 'checked="true"'; ?> />
    <?php make_number_select("hora_inicio_h", 0, 23, 0); ?>
    <?php make_number_select("hora_inicio_m", 0, 59, 0); ?>
      <label class="frm" style="width: auto; margin-left: 20px; " for="hora_fin_h">hasta:</label>
      <input class="cb" type="checkbox" id="hora_fin" name="hora_fin"
             value="1"
	     <?php if ($_GET['hora_fin'] == "1")
                     echo 'checked="true"'; ?> />
      <?php make_number_select("hora_fin_h", 0, 23, 23); ?>
      <?php make_number_select("hora_fin_m", 0, 59, 59); ?>
      <br />
      <label class="frm" for="recepcion">Medio de recepci&oacute;n:</label>
      <?php make_select('recepcion', 'medios_de_recepcion', $_GET, true); ?>
      <label class="frm" for="estado">Estado:</label>
      <select class="frm" id="estado" name="estado">
	<?php
	   make_option("estado", "-", "-1", $_GET);
	   make_option("estado", "Abierto", "true", $_GET);
	   make_option("estado", "Cerrado", "false", $_GET);
	?>
      </select><br />
      <label class="frm" for="suceso">Tipo de suceso:</label>
      <?php make_select('suceso', 'tipos_de_suceso', $_GET, true); ?>
      <br />
      <label class="frm" for="tipo_registro">Tipo de registro:</label>
      <select class="frm" id="tipo_registro" name="tipo_registro">
	<?php
           make_option("tipo_registro", "-", "-1", $_GET);
           make_option("tipo_registro", "Ver&iacute;dico", "0", $_GET);
           make_option("tipo_registro", "Accidental", "1", $_GET);
        ?>
      </select>
      <label class="frm" for="canalizacion">Canalizaci&oacute;n:</label>
      <?php make_select('canalizacion', 'canalizaciones', $_GET, true); ?>
      <br />
      <label class="frm" for="turno">Turno:</label>
      <select class="frm" id="turno" name="turno">
	<?php
	   make_option("turno", "-", "-1", $_GET);
           make_option("turno", "Matutino", "0", $_GET);
           make_option("turno", "Vespertino", "1", $_GET);
           make_option("turno", "Nocturno", "2", $_GET);
	?>
      </select>
      <br />
      <label class="frm" for="usuario_crea">Usuario que hizo la captura:</label>
      <?php make_input('usuario_crea', $_GET); ?>
      <label class="frm" for="usuario_cierra">Usuario que cerr&oacute;:</label>
      <?php make_input('usuario_cierra', $_GET); ?>
      <br />
      <label class="frm" for="cuenta">ID de alarma:</label>
      <?php make_input('cuenta', $_GET); ?>
      <label class="frm" for="colonia">Colonia:</label>
      <?php make_input('colonia', $_GET); ?>
      <br />
      <label class="frm" for="sector">Sector:</label>
      <?php make_select('sector','sectores', $_GET, true); ?>
      <label class="frm" for="telefono">Tel&eacute;fono:</label>
      <?php make_input('telefono', $_GET); ?>
      <br />
      <label class="frm" for="camara">C&aacute;mara:</label>
      <?php make_select('camara', 'camaras', $_GET, true); ?>
      <br />
      <input id="buscar" name="buscar" type="submit" value="Buscar" />
      <br />
      </div>
    </form>
    <?php
      if ($_GET['buscar'])
	{
	  $url = "buscar_suceso_relevante.php?" . urlencode(http_build_query($_GET));
	  echo "<a target=\"_blank\" href=\"make_pdf.php?url=$url\">Exportar a PDF</a>";
	}
      }
    ?>
    <?php if ($_GET['pdf']) { echo '<center><h3>Sucesos relevantes</h3></center>' . "\n"; } ?>
    <hr />
<?php

if ($results)
  {
    $headers = array("ID", "Fecha y Hora", "Medio de recepci&oacute;n", "Suceso", "Estado");

    echo '<div id="search_results">' . "\n";
    echo '<table id="search_results">' . "\n";
    echo '<tr class="header">';
    
    $medios_de_recepcion = get_poplist_table_array("medios_de_recepcion");
    $tipos_de_suceso = get_poplist_table_array("tipos_de_suceso");

    $num = 0;

    while ($row = mysql_fetch_row($results))
      {
	foreach ($headers as $hd)
	  echo '<th class="header">' . $hd . '</th>';

	echo '<tr onClick="window.open(\'ver_suceso.php?id=' .
	  $row[0] . '\', \'Suceso\', \'width=730,height=700,status=0,toolbar=0\');" onmouseover="this.style.backgroundColor = \'#ffffff\';" onmouseout="this.style.backgroundColor = \'#f5f1e8\';" class="result">';

	echo '<td class="result_cell">' . $row[0] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[1] . " " . $row[2] . '</td>' . "\n";
	echo '<td class="result_cell">' . uhtmlentities($medios_de_recepcion[$row[3]]) . '</td>' . "\n";
	echo '<td class="result_cell">' . uhtmlentities($tipos_de_suceso[$row[4]]) . '</td>' . "\n";
	echo '<td class="result_cell">' . ($row[5] ? "Abierto" : "Cerrado") . '</td>' . "\n";
	echo '</tr>' . "\n";
	
	if ($row["notas_abre"])
	  echo '<tr><td class="note_cell" colspan="5"><b>Notas al abrir:</b> ' . uhtmlentities($row[6]) . '</td></tr>' . "\n";
	if ($row["notas_cierra"])
	  echo '<tr><td class="note_cell" colspan="5"><b>Notas al cerrar:</b> ' . uhtmlentities($row[7]) . '</td></tr>' . "\n";
	echo '<tr><td class="note_cell" colspan="5"><hr /></td></tr>' . "\n";
	$num += 1;
      }
    echo '</table>' . "\n";
    echo "<br /><p>Total de registros: $num</p>";
    echo '</div>' . "\n";
  }

include("footer.html");

end_connection($con);
?>
