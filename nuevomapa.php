<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('session.inc');
include("header.html");
?>
	      <div id="title">
      <span id="title_left">
	  
        <a class="title_link" href="./">inicio</a>: 
        <a class="title_link" href="localizacion.php"></a>
      </span>
      <span id="title_center">GeoLocalizacion</span>
    </div>
    <style>
        #mapa{
            width: 905px;
            height: 600px;
            float:left;
            background: #f4f1e8;
        }
        #infor{
            width: 400px;
            height: 400px;
            float:left;
			background:#f4f1e8;
        }
    </style>
<!--IMPORTANTE RESPETAR EL ORDEN -->
<!--ESTILOS DE BOOSTRAP -->
<link href="api/css/calendario.css" rel="stylesheet" />
<link href="api/css/bootstrap.min.css" rel="stylesheet" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js" ></script>
<!--ARCHIVOS JAVASCRIPT DE BOOTSTRAP -->
<script type="text/javascript" src="api/js/bootstrap.min.js" ></script>
<!--esto servira para hacer lo del calendario-->
<link href="calendario_dw/calendario_dw-estilos.css" type="text/css" rel="STYLESHEET"> 
<script type="text/javascript" src="calendario_dw/calendario_dw.js"></script> 

<script type="text/javascript">
   $(document).ready(function(){
      $(".campofecha").calendarioDW();
   })
   </script>





<script>
    //VARIABLES GENERALES
		//declaras fuera del ready de jquery
    var nuevos_marcadores = [];
    var marcadores_bd= [];






    var mapa = null; //VARIABLE GENERAL PARA EL MAPA
    //FUNCION PARA QUITAR MARCADORES DE MAPA
    function limpiar_marcadores(lista)
    {
        for(i in lista)
        {
            //QUITAR MARCADOR DEL MAPA
            lista[i].setMap(null);
        }
    }
    $(document).on("ready", function(){
        
        //VARIABLE DE FORMULARIO
        var formulario = $("#formulario");
        
        var punto = new google.maps.LatLng(16.843976,-99.873619);
        var config = {
            zoom:13,
            center:punto,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        mapa = new google.maps.Map( $("#mapa")[0], config );

        google.maps.event.addListener(mapa, "click", function(event){
           var coordenadas = event.latLng.toString();
           
           coordenadas = coordenadas.replace("(", "");
           coordenadas = coordenadas.replace(")", "");
           
           var lista = coordenadas.split(",");
           
           var direccion = new google.maps.LatLng(lista[0], lista[1]);
           //PASAR LA INFORMACI?N AL FORMULARIO
           formulario.find("input[name='titulo']").focus();
		       formulario.find("input[name='fecha']").focus();
           formulario.find("input[name='cx']").val(lista[0]);
           formulario.find("input[name='cy']").val(lista[1]);
           
           
           
           
           google.maps.event.addListener(marcador, "click", function(){

           });
           
           //BORRAR MARCADORES NUEVOS
           limpiar_marcadores(nuevos_marcadores);
           marcador.setMap(mapa);
        });
        $("#btn_grabar").on("click", function(){
            //INSTANCIAR EL FORMULARIO
            var f = $("#formulario");
            
            //VALIDAR CAMPO TITULO
            if(f.find("input[name='titulo']").val().trim()=="")
            {
                alert("Falta titulo");
                return false;
            }
			       //VALIDAR CAMPO FECHA
            if(f.find("input[name='fecha']").val().trim()=="")
            {
                alert("Falta fecha");
                return false;
            }
            //VALIDAR CAMPO CX
            if(f.find("input[name='cx']").val().trim()=="")
            {
                alert("Falta Coordenada X");
                return false;
            }
            //VALIDAR CAMPO CY
            if(f.find("input[name='cy']").val().trim()=="")
            {
                alert("Falta Coordenada Y");
                return false;
            }
            //FIN VALIDACIONES
            
            if(f.hasClass("busy"))
            {
                //Cuando se haga clic en el boton grabar
                //se le marcar? con una clase 'busy' indicando
                //que ya se ha presionado, y no permitir que se
                //realiCe la misma operaci?n hasta que esta termine
                //SI TIENE LA CLASE BUSY, YA NO HARA NADA
                return false;
            }
            //SI NO TIENE LA CLASE BUSY, SE LA PONDREMOS AHORA
            f.addClass("busy");
            //Y CUANDO QUITAR LA CLASE BUSY?
            //CUANDO SE TERMINE DE PROCESAR ESTA SOLICITUD
            //ES DECIR EN EL EVENTO COMPLETE
            
            var loader_grabar = $("#loader_grabar");

           $.ajax({
               type:"POST",
               url:"api/iajax.php",
               dataType:"JSON",
               data:f.serialize()+"&tipo=grabar",
               success:function(data){
                   if(data.estado=="ok")
                    {
                        loader_grabar.removeClass("label-warning").addClass("label-success")
                        .text("Grabado OK").delay(4000).slideUp();
                        listar();
                    }
                    else
                    {
                        alert(data.mensaje);
                    }
                   
                   
               },
               beforeSend:function(){
                   //Notificar al usuario mientras que se procesa su solicitud
                   loader_grabar.removeClass("label-success").addClass("label label-warning")
                   .text("Procesando...").slideDown();
               },
               complete:function(){
                   //QUITAR LA CLASE BUSY
                   f.removeClass("busy");
                   f[0].reset();
                   //[0] jquery trabaja con array de elementos javascript no
                   //asi que se debe especificar cual elemento se har? reset
                   //capricho de javascript
                   //AHORA PERMITIR? OTRA VEZ QUE SE REALICE LA ACCION
                   //Notificar que se ha terminado de procesar
                   
               }
           });
           return false;
        });
        //BORRAR
        $("#btn_borrar").on("click", function(){
            //CONFIRMAR ACCION DEL USUARIO
            if(confirm("Está seguro?")==false)
            {
                //NO HARA NADA
                //alert("que madres?");
                return;
            }

            var formulario_edicion = $("#formulario_edicion");
            alert(formulario_edicion.serialize());
            $.ajax({
                type:"POST",
                url:"api/iajax.php",
                data:formulario_edicion.serialize()+"&tipo=borrar",
                dataType:"JSON",
                success:function(data){
                    //SABER CUANDO SE BORRÓ CORRECTAMENTE
                    if(data.estado=="ok")
                    {
                        //MOSTRAR EL MENSAJE
                        alert(data.mensaje);
                        //BORRAR MARCADORES NUEVOS EN CASO DE QUE HUBIESEN
                        limpiar_marcadores(nuevos_marcadores);
                        //LIMPIAR EL FORMULARIO
                        formulario_edicion[0].reset();
                        //LISTAR OTRA VEZ LOS MARCADORES
                        listar();
                    }
                    else
                    {
                        //ERROR AL BORRAR
                        alert(data.mensaje);
                    }
                    
                },
                beforeSend:function(){
                    
                },
                complete:function(){
                    
                }
            });
        });
        
        //CARGAR PUNTOS AL TERMINAR DE CARGAR LA P?GINA
        listar();//FUNCIONA, AHORA A GRAFICAR LOS PUNTOS EN EL MAPA
    });
    //FUERA DE READY DE JQUERY
    //FUNCTION PARA RECUPERAR PUNTOS DE LA BD

    function listar_algunos(){

            
            var fec1 = document.getElementById("fecha1").value;
            var fec2 = document.getElementById("fecha2").value;

            var fechaprimera = fec1.split('/');
            var fechasegunda = fec2.split('/');

            var fp1 = fechaprimera[0];
            var afp1 = fechaprimera[1];
            var afp2 = fechaprimera[2];
            var fp2 = fechasegunda[0];
            var bfp1 = fechasegunda[1];
            var bfp2 = fechasegunda[2];
            var fecha1gmx;
            var fecha2gmx;

            if(fp1.length==1){
              fecha1gmx = afp2+"-"+afp1+"-"+"0"+fp1;
              //alert("La primer fecha queda "+fecha1gm);
                
           }
           
           else{
              fecha1gmx = afp2+"-"+afp1+"-"+fp1;
              //alert("La fecha queda asi: "+fecha1gm);
           }

            
           if(fp2.length==1){
            fecha2gmx = bfp2+"-"+bfp1+"-"+"0"+fp2;
              //alert("La segunda fecha queda "+fecha2gm);
           }
           else{
            fecha2gmx = bfp2+"-"+bfp1+"-"+fp2;
              //alert("La fecha queda asi: "+fecha2gm);
           }

      limpiar_marcadores(marcadores_bd);
      
     
      //var formData = {fecha1gm:""+fecha1gm,fecha2gm:""+fecha2gm}; //Array
      //alert(formData.serialize());
      //alert(fecha1gmx);
      var perro = fecha1gmx+"&"+fecha2gmx;
      //alert(perro);
      

       $.ajax({
               type:"POST",
               url:"api/iajax.php",
               dataType:"JSON",
              
               //data: {'fecha1gm': fecha1gmx,'fecha2gm': fecha2gmx}+"&tipo=muestra",
               //data:perro.serialize()+"&tipo=muestra",
               data: perro + "&tipo=muestra",
               
               success:function(data){
                   if(data.estado=="ok")
                    {
                        //alert("Hay puntos en la BD");
                        $.each(data.mensaje, function(i, item){
                            //OBTENER LAS COORDENADAS DEL PUNTO
                            var posi = new google.maps.LatLng(item.cx, item.cy);//bien
                            //CARGAR LAS PROPIEDADES AL MARCADOR
                            var marca = new google.maps.Marker({
                                idMarcador:item.IdPunto,
                                position:posi,
                                titulo: item.Titulo,
                                fecha:  item.fecha,
                                cx:item.cx,//esas coordenadas vienen de la BD
                                cy:item.cy//esas coordenadas vienen de la BD
                            });
                            //AGREGAR EVENTO CLICK AL MARCADOR
                            //MARCADORES QUE VIENEN DE LA BASE DE DATOS
                            google.maps.event.addListener(marca, "click", function(){
                               //ENTRAR EN EL SEGUNDO COLAPSIBLE
                               //Y OCULTAR EL PRIMERO
                               $("#collapseTwo").collapse("show");
                               $("#collapseOne").collapse("hide");
                               //VER DOCUMENTACIÓN DE BOOTSTRAP :)
                               
                               //AHORA PASAR LA INFORMACIÓN DEL MARCADOR
                               //AL FORMUALARIO
                               formulario_edicion.find("input[name='id']").val(marca.idMarcador);
                               formulario_edicion.find("input[name='titulo']").val(marca.titulo).focus();
                               formulario_edicion.find("input[name='fecha']").val(marca.fecha).focus();
                               formulario_edicion.find("input[name='cx']").val(marca.cx);
                               formulario_edicion.find("input[name='cy']").val(marca.cy);
                               
                            });
                            //AGREGAR EL MARCADOR A LA VARIABLE MARCADORES_BD
                            marcadores_bd.push(marca);
                            //UBICAR EL MARCADOR EN EL MAPA
                            marca.setMap(mapa);
                        });
                    }
                else
                    {
                        //alert(data);
                        alert("la que metiste "+fecha1gmx + " y "+fecha2gmx);
                        alert(data.mensaje);

                        
                       
                    }
               },
               beforeSend:function(){
                  
               },
               complete:function(){
                   
               }
           });
      

    }

    

    function listar()
    {
        //ANTES DE LISTAR MARCADORES
        //SE DEBEN QUITAR LOS ANTERIORES DEL MAPA
       limpiar_marcadores(marcadores_bd);
       var formulario_edicion = $("#formulario_edicion");
       $.ajax({
               type:"POST",
               url:"api/iajax.php",
               dataType:"JSON",
               data:"&tipo=listar",
               success:function(data){
                   if(data.estado=="ok")
                    {
                        //alert("Hay puntos en la BD");
                        $.each(data.mensaje, function(i, item){
                            //OBTENER LAS COORDENADAS DEL PUNTO
                            var posi = new google.maps.LatLng(item.cx, item.cy);//bien
                            //CARGAR LAS PROPIEDADES AL MARCADOR
                            var marca = new google.maps.Marker({
                                idMarcador:item.IdPunto,
                                position:posi,
                                titulo: item.Titulo,
								fecha:  item.fecha,
                                cx:item.cx,//esas coordenadas vienen de la BD
                                cy:item.cy//esas coordenadas vienen de la BD
                            });
                            //AGREGAR EVENTO CLICK AL MARCADOR
                            //MARCADORES QUE VIENEN DE LA BASE DE DATOS
                            google.maps.event.addListener(marca, "click", function(){
                               //ENTRAR EN EL SEGUNDO COLAPSIBLE
                               //Y OCULTAR EL PRIMERO
                               $("#collapseTwo").collapse("show");
                               $("#collapseOne").collapse("hide");
                               //VER DOCUMENTACIÓN DE BOOTSTRAP :)
                               
                               //AHORA PASAR LA INFORMACIÓN DEL MARCADOR
                               //AL FORMUALARIO
                               formulario_edicion.find("input[name='id']").val(marca.idMarcador);
                               formulario_edicion.find("input[name='titulo']").val(marca.titulo).focus();
							                 formulario_edicion.find("input[name='fecha']").val(marca.fecha).focus();
                               formulario_edicion.find("input[name='cx']").val(marca.cx);
                               formulario_edicion.find("input[name='cy']").val(marca.cy);
                               
                            });
                            //AGREGAR EL MARCADOR A LA VARIABLE MARCADORES_BD
                            marcadores_bd.push(marca);
                            //UBICAR EL MARCADOR EN EL MAPA
                            marca.setMap(mapa);
                        });
                    }
                else
                    {
                        alert("NO hay puntos en la BD");
                    }
               },
               beforeSend:function(){
                   
               },
               complete:function(){
                   
               }
           });
    }
    //PLANTILLA AJAX
    
</script>
</head>
<body>
    <div id="mapa">
        <h2>Aqui irá el mapa!</h2>
    </div>
    <div id="infor">
        <div class="accordion" id="accordion2">
            <div class="accordion-group">
              <div class="accordion-heading">
                
              </div>
              <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <form id="formulario">
                        <table>
                          ssx-borraralratito
                            
                        </table>
                    </form>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                
              </div>
              <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                  <form id="formulario_edicion">
                      <!-- CAMPO OCULTO PARA LA VARIABLE ID -->
                      <input type="hidden" name="id"/>
                        <table>
                            <tr>
                                <td>T&#237;tulos</td>
                                <td><input type="text" class="form-control"  name="titulo" autocomplete="off"/></td>
                            </tr>
							<tr>
							<td>Fecha</td>
							    <td><input type="text"  class="form-control" name="fecha"  autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td>Coordenadas X</td>
                                <td><input type="text" class="form-control" readonly  name="cx" autocomplete="off"/></td>
                            </tr>
                            <tr>
                                <td>Coordenada Y</td>
                                <td><input type="text" class="form-control"  readonly name="cy" autocomplete="off"/></td>
                            </tr>
                            <!-- Aqui estar? se colocaran los mensajes para el usuario -->
                            <tr>
                                <td></td>
                                <td><span id="loader_grabar" class=""></span></td>
                            </tr>
                            <tr>
                                
                                <td><button type="button" id="btn_borrar" class="btn btn-danger btn-sm">Borrar</button></td>
                            </tr>
                        </table>
                    </form>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                
              </div>
              <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                  ...
                </div>
              </div>
            </div>
          </div>
          <div class="Calendariecito">
            <p>Selecciona el Rango de Fechas para generar el Reporte</p>
            <br/>
            <p class="hola">Fecha de Inicio: <input type="text" class="campofecha" name="fecha1" id="fecha1" size="14"></p>
            <br/>
            <p>Fecha Finalización: <input type="text" class="campofecha" name="fecha2" id="fecha2" size="14"></p>
            <p><input class="gobutton" onclick="listar_algunos();" type="submit" value="Generar Reporte"/> </p>

          </div>







    </div>
	
	<?php
include("footer.html");
?>