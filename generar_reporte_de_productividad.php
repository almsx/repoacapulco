<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('utils.inc');

$result = NULL;
$con = make_connection();

$fecha_inicio = $_GET['fecha_inicio'];
$fecha_fin = $_GET['fecha_fin'];

$nmedios = get_poplist_table_array("medios_de_recepcion");
$nusuarios = $_GET['users'];

if ($_GET['dependencia'])
  {
    $nusuarios = array();
    $result = mysql_query('SELECT CONCAT(nombre, " - ", nombre_persona, " ", apellidos) FROM usuarios WHERE dependencia LIKE "%' . $_GET['dependencia'] . '%";');
    
    while ($row = mysql_fetch_row($result))
      $nusuarios[] = $row[0];
  }

function only_login($str)
{
  $res = split(' - ', $str);

  return $res[0];
}

$nlogins = array_map("only_login", $nusuarios);
$nusuarios = array_combine($nlogins, $nusuarios);

$counts = array();

foreach (array_keys($nmedios) as $id)
  $counts[] = "SUM(medio_de_recepcion_id = $id)";

$counts_sel = implode(", ", $counts);

$cond = "WHERE s.fecha >= '$fecha_inicio' AND s.fecha <= '$fecha_fin'";

if (0 < strlen($_GET['dependencia']))
  $cond .= ' AND u.dependencia LIKE "%' . $_GET['dependencia'] . '%"';
if ($_GET['turno'] != -1)
  $cond .= ' AND s.turno = ' . $_GET['turno'];

$cmedios_query = "SELECT s.usuario_crea, COUNT(*) FROM sucesos AS s LEFT JOIN usuarios AS u ON s.usuario_crea = u.nombre $cond GROUP BY usuario_crea ORDER BY usuario_crea ASC;";
$cmedios_por_usuario_query = "SELECT s.usuario_crea, COUNT(*), $counts_sel FROM sucesos as s LEFT JOIN usuarios AS u ON s.usuario_crea = u.nombre $cond GROUP BY usuario_crea ORDER BY usuario_crea ASC;";

$res = mysql_query($cmedios_query);
if (!$res) die("No hubieron resultados");
$cmedios_result = result_to_array($res);
$res = mysql_query($cmedios_por_usuario_query);
$cmedios_por_usuario_result = result_to_array($res);



function make_ref_img($i)
{
  $ref = $i * 3 + 2;

  return "<img src=\"./gp/graphref.php?ref=$ref&typ=2&dim=6\" />";  
}

function print_sucesos_graph($r)
{
  global $nlogins;
  global $nusuarios;
  $usuarios = array();
  $vals = array();

  foreach ($r as $row)
    $usuarios[$row[0]] = $row[1];  

  foreach ($nlogins as $u)
    $vals[$u] = ($usuarios[$u] ? $usuarios[$u] : 0);

  $imgurl = './gp/graphbarras.php?dat=' . implode(",", array_values($vals));
  echo '<img src="' . $imgurl . '" />' . "\n";
}

function print_sucesos_usuarios_graphs($r)
{
  global $nmedios;
  global $nlogins;
  global $nusuarios;
  $ctotal = 0;
  $ids = array_keys($nmedios);
  $usuarios = array();
  $totales = array();

  foreach($nlogins as $u)
    {
      $name = $nusuarios[$u];
      $usuarios[$u][0] = $name;
    }

  foreach($r as $row)
    {
      if ($usuarios[$row[0]] != NULL)
	$usuarios[$row[0]] = $row;
    }
  
  $i = 0;

  foreach ($usuarios as $row)
    {
      $name = urlencode($nusuarios[$row[0]] ? $nusuarios[$row[0]] : $row[0]);
      $imgurl = "./gp/graphbarras.php?ttl=$name&dat=";
      foreach (array_keys($ids) as $x)
	{
	  $n = $row[$x+2] ? $row[$x+2] : 0;
	  $imgurl .= "$n,";
	}
      $total = $row[1] ? $row[1] : 0;
      $imgurl .= "$total";
      if (($i % 4) == 0)
	{
	  echo "<!-- PAGE BREAK -->\n" .
	    "<tr>\n" .
	    "<td>\n" .
	    "<center><img src=\"logo_pdm.jpg\" /></center>\n" .
	    "</td>\n" .
	    "<td>\n" .
	    "<center><img src=\"logo_care.jpg\" /></center>\n" .
	    "</td>\n" .
	    "</tr>\n";
	}

      if ($i % 2)
	echo '</td><td>';
      else
	echo '<tr><td>';

      echo '<img src="' . $imgurl . '" />' . "\n";

      if ($i % 2)
	echo '</tr>';

      $i += 1;

      if (($i % 4) == 0 && $i != 0)
	{
	  echo '<tr><td colspan="2"><center><table>';
	  
	  $x = 1;
	  
	  foreach ($nmedios as $nm)
	    {
	      $refimg = make_ref_img($x % 10);
	      $name = uhtmlentities($nm);
	      echo "<tr><td>$refimg</td><td>$name</td></tr>";
	      $x += 1;
	    }
	  
	  $refimg = make_ref_img($x % 10);
	  echo "<tr><td>$refimg</td><td>Total</td></tr>";
	  echo '</table></center></td></tr>';
	}
    }
}

function print_sucesos_table($r)
{
  global $nmedios;
  global $nlogins;
  global $nusuarios;
  $ctotal = 0;
  $ids = array_keys($nmedios);
  $usuarios = array();
  $totales = array();

  foreach($nlogins as $u)
    {
      $name = $nusuarios[$u];
      $usuarios[$u][0] = $name;
    }

  foreach($r as $row)
    {
      if ($usuarios[$row[0]] != NULL)
	$usuarios[$row[0]] = $row;
    }

  echo '<table border="1"><tr><th></th><th>Usuario</th>';

  foreach($nmedios as $name)
    {
      $name = uhtmlentities($name);
      echo "<th>$name</th>";
    }

  echo '<th>Total</th></tr>';

  $i = 1;

  foreach ($usuarios as $row)
    {
      $name = uhtmlentities($row[1] ? $nusuarios[$row[0]] : $row[0]);
      $img = make_ref_img($i % 10);
      $i += 1;
      echo "<tr><td>$img</td><td>$name</td>";
      foreach (array_keys($ids) as $x)
	{
	  $n = $row[$x+2] ? $row[$x+2] : 0;
	  echo "<td align=\"right\">$n</td>";
	  $totales[$ids[$x]] += $n;
	}
      $total = $row[1] ? $row[1] : 0;
      $ctotal += $total;
      echo "<td align=\"right\">$total</td>";
      echo '</tr>';
    }

  echo "<tr><td></td><td>Total</td>";
  foreach (array_keys($nmedios) as $k)
    {
      $n = $totales[$k] ? $totales[$k] : 0;
      echo "<td align=\"right\">$n</td>";
    }

  echo "<td align=\"right\">$ctotal</td></tr>";
  echo '</table>';
}

$meses = array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre");
sscanf($_GET['fecha_inicio'], "%d-%d-%d", $y, $m, $d);
$mes = $meses[$m];
$fecha_inicio = "$d de $mes del $y";
sscanf($_GET['fecha_fin'], "%d-%d-%d", $y, $m, $d);
$mes = $meses[$m];
$fecha_final = "$d de $mes del $y";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head><title>Informe</title></head>
  <body>
    <table cellpadding="10" cellspacing="10" align="center">
      <tr>
	<td>
	  <center><img src="logo_pdm.jpg" /></center>
	</td>
	<td>
	  <center><img src="logo_care.jpg" /></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php echo "Desde $fecha_inicio hasta el $fecha_final"; ?></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
          <center><h4>INFORME DE PRODUCTIVIDAD DEL CENTRO DE AZCAPOTZALCO DE RESPUESTA A EMERGENCIA</h4></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php print_sucesos_graph($cmedios_result); ?></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
          <center><?php print_sucesos_table($cmedios_por_usuario_result); ?></center>
	</td>
      </tr>
      <?php print_sucesos_usuarios_graphs($cmedios_por_usuario_result); ?>
    </table>
  </body>
</html>
<?php

if ($con)
  end_connection($con);
?>
