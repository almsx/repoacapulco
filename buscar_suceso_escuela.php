<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

if (!$_GET['pdf'])
  include('session.inc');
include("utils.inc");

$con = make_connection();

$results = NULL;

if ($_GET['buscar'])
  {
    $filters = array();
    $query = NULL;

    if ($_GET['fecha_inicio'] && $_GET['fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha >= ' . sqlquote($_GET['fecha_inicio']);
    if ($_GET['fecha_fin'] && $_GET['fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha <= ' . sqlquote($_GET['fecha_fin']);

    $query = "SELECT s.id, s.medio_de_recepcion_id, CONCAT(s.fecha, ' ', s.hora)AS fecha, s.turno, s.abierto, s.notas_abre, s.notas_cierra, (SELECT descripcion FROM canalizaciones WHERE id = s.canalizacion_id) AS canalizacion, s.proteccion_civil, s.seguridad_publica, s.tiempo_care, s.tiempo_base_plata, s.suceso_relevante, s.abierto, CONCAT(s.usuario_crea, ' ', s.perfil_usuario_crea) AS usuario_crea, CONCAT(s.usuario_cierra, ' ', s.perfil_usuario_crea) AS usuario_crea, s.tipo_de_registro_id, a.cuenta, a.cap_code, r.direccion, r.colonia, (SELECT descripcion FROM sectores WHERE id = r.sector_id) AS sector, telefono, calles, (SELECT descripcion FROM camaras WHERE id = m.camara_id) AS camara FROM sucesos AS s LEFT JOIN eventos_alarmas AS a ON s.id = a.id LEFT JOIN eventos_radio_telefono AS r ON s.id = r.id LEFT JOIN eventos_monitores AS m ON s.id = m.id";

    if (count($filters) > 0)
      $filterstr .= "s." . implode(" AND s.", $filters);
    
    $query .= (strlen($filterstr) > 0 ? " WHERE tipo_de_suceso_id = 17 AND $filterstr;" : ' WHERE s.tipo_de_suceso_id = 17;');
    $results = mysql_query($query);
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");

?>
    <?php if (!$_GET['pdf']) { ?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="reportes.php">Reportes</a>:
      </span>
      <span id="title_center">Seguimientos de escuela segura</span>
    </div>
    <form action="buscar_suceso_escuela.php" method="GET">
      <div id="form">
      <label class="frm" for="fecha_inicio">Fecha inicial:</label>
      <script>DateInput('fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="fecha_fin">Fecha final:</label>
      <script>DateInput('fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <input id="buscar" name="buscar" type="submit" value="Buscar" />
      <br />
      </div>
    </form>
    <?php
      if ($_GET['buscar'])
	{
	  $url = "buscar_suceso_escuela.php?" . urlencode(http_build_query($_GET));
	  echo "<a target=\"_blank\" href=\"make_pdf.php?url=$url\">Exportar a PDF</a>";
	}
      }
    ?>
    <?php
    if ($_GET['pdf'])
      {
	$meses = array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre");
	sscanf($_GET['fecha_inicio'], "%d-%d-%d", $y, $m, $d);
	$mes = $meses[$m];
	$fecha_inicio = "$d de $mes del $y";
	sscanf($_GET['fecha_fin'], "%d-%d-%d", $y, $m, $d);
	$mes = $meses[$m];
	$fecha_final = "$d de $mes del $y";

	echo "<center><h3>Seguimiento de escuelas seguras</h3></center>\n";
	echo "<center>Desde $fecha_inicio hasta el $fecha_final</center><br />\n";
      }
    ?>
    <hr />
<?php

function ct($str)
{
  return ($_GET["pdf"] ? "<center>$str</center>" : $str);
}

if ($results)
  {
    $headers = array("ID", "Fecha y Hora", "Medio de recepci&oacute;n", "Estado");
    $headers2 = array("Canalizaci&oacute;n", "Protecci&oacute;n Civil", "Seguridad Publica", "Turno");
    $headers3 = array("Tiempo Care", "Tiempo Base Plata", "Suceso Relevante", "Usuarios");

    echo '<div id="search_results">' . "\n";
    echo '<table id="search_results"' . ($_GET['pdf'] ? 'width="700"' : "") . '>' . "\n";
    echo '<tr class="header">';
    
    $medios_de_recepcion = get_poplist_table_array("medios_de_recepcion");

    $num = 0;

    while ($row = mysql_fetch_array($results))
      {
	$turnos = array("Matutino", "Vespertino", "Nocturno");

	foreach ($headers as $hd)
	  echo '<th class="header">' . $hd . '</th>';

	echo '<tr onClick="window.open(\'ver_suceso.php?id=' .
	  $row[0] . '\', \'Suceso\', \'width=730,height=700,status=0,toolbar=0\');" onmouseover="this.style.backgroundColor = \'#ffffff\';" onmouseout="this.style.backgroundColor = \'#f5f1e8\';" class="result">';

	echo '<td class="result_cell">' . ct($row["id"]) . "</td>\n";
	echo '<td class="result_cell">' . ct($row["fecha"]) . "</td>\n";
	echo '<td class="result_cell">' . ct(uhtmlentities($medios_de_recepcion[$row["medio_de_recepcion_id"]])) . "</td>\n";
	echo '<td class="result_cell">' . ct($row["abierto"] ? "Abierto" : "Cerrado") . "</td>\n";
	echo "</tr>\n";

	echo "<tr class=\"result\">\n";
	foreach ($headers2 as $hd)
	  echo '<th class="header">' . $hd . '</th>';
	echo "</tr>\n";

	echo "<tr class=\"result\">\n";
	echo '<td class="result_cell">' . ct(uhtmlentities($row["canalizacion"])) . "</td>\n";
	echo '<td class="result_cell">' . ct(uhtmlentities($row["proteccion_civil"])) . "</td>\n";
	echo '<td class="result_cell">' . ct(uhtmlentities($row["seguridad_publica"])) . "</td>\n";
	echo '<td class="result_cell">' . ct($turnos[$row["turno"]]) . "</td>\n";
	echo "</tr>\n";

	echo "<tr class=\"result\">\n";
	foreach ($headers3 as $hd)
	  echo '<th class="header">' . $hd . '</th>';
	echo "</tr>\n";

	echo "<tr class=\"result\">\n";
	echo '<td class="result_cell">' . ct($row["tiempo_care"]) . "</td>\n";
	echo '<td class="result_cell">' . ct($row["tiempo_base_plata"]) . "</td>\n";
	echo '<td class="result_cell">' . ct($row["suceso_relevante"] ? "Si" : "No") . "</td>\n";
	echo '<td class="result_cell">' . ct(uhtmlentities($row["usuario_crea"] . " " . $row["usuario_cierra"])) . "</td>\n";
	echo "</tr>\n";

	switch($row["medio_de_recepcion_id"])
	  {
	  case 1:
	    $headers4 = array("ID de alarma", "Cap Code");
	    echo "<tr class=\"result\">\n";
	    foreach ($headers4 as $hd)
	      echo '<th class="header" colspan="2">' . $hd . '</th>';
	    echo "</tr>\n";

	    echo "<tr class=\"result\">\n";
	    echo '<td class="result_cell" colspan="2">' . ct($row["cuenta"]) . "</td>\n";
	    echo '<td class="result_cell" colspan="2">' . ct($row["cap_code"]) . "</td>\n";
	    echo "</tr>\n";

	    break;
	  case 2:
	  case 3:
	    $headers4 = array("Colonia", "Sector", "Direcci&oacute;n");
	    
	    if ($row["medio_de_recepcion_id"] == "2")
	      $headers4[] = "Tel&eacute;fono";
	    else
	      $headers4[] = "";

	    echo "<tr class=\"result\">\n";
	    foreach ($headers4 as $hd)
	      echo '<th class="header">' . $hd . '</th>';
	    echo "</tr>\n";

	    echo "<tr class=\"result\">\n";
	    echo '<td class="result_cell">' . ct($row["colonia"]) . "</td>\n";
	    echo '<td class="result_cell">' . ct($row["sector"]) . "</td>\n";
	    echo '<td class="result_cell">' . ct($row["direccion"] . "<br />" . $row["calles"]) . "</td>\n";

	    if ($row["medio_de_recepcion_id"] == "2")
	      {
		echo '<td class="result_cell">' . ct($row["telefono"]) . "</td>\n";
	      }
	    echo "</tr>\n";

	    break;
	  case 4:
	    $headers4 = array("C&aacute;mara");

	    echo "<tr class=\"result\">\n";
	    foreach ($headers4 as $hd)
	      echo '<th class="header" colspan="4">' . $hd . '</th>';
	    echo "</tr>\n";

	    echo "<tr class=\"result\">\n";
	    echo '<td class="result_cell" colspan="4">' . ct($row["camara"]) . "</td>\n";
	    echo "</tr>\n";

	    break;
	  }

	if ($row["notas_abre"])
	  echo '<tr><td class="note_cell" colspan="5"><b>Notas al abrir:</b> ' . uhtmlentities($row["notas_abre"]) . '</td></tr>' . "\n";
	if ($row["notas_cierra"])
	  echo '<tr><td class="note_cell" colspan="5"><b>Notas al cerrar:</b> ' . uhtmlentities($row["notas_cierra"]) . '</td></tr>' . "\n";
	echo '<tr><td class="note_cell" colspan="5"><hr /></td></tr>' . "\n";

	$num += 1;
      }
    echo '</table>' . "\n";
    echo "<br /><p>Total de registros: $num</p>";
    echo '</div>' . "\n";
  }

include("footer.html");

end_connection($con);
?>
