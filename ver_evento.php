<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

include('session.inc');
include('utils.inc');

$noexit = true;
$print = true;

include('header.html');

$result = NULL;
$con = NULL;

$id = $_GET['id'];

if ($id)
  {
    $con = make_connection();

    $query = 'SELECT id, fecha, hora, fecha_evento, hora_evento, tipo, lugar, visitante, escuela_segura, usuario_crea, notas FROM eventos WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }
?>
<div id="title"><span id="title_center">Ver evento</span></div>
<?php

if ($result)
  {
    $row = mysql_fetch_row($result);

    $v = array('id' => $row[0],
	       'fecha' => $row[1] . ' ' . $row[2],
	       'fecha_evento' => $row[3] . ' ' . $row[4],
	       'tipo' => $row[5],
	       'lugar' => $row[6],
	       'visitante' => $row[7],
	       'escuela_segura' => ($row[8] ? "Si" : "No"),
	       'usuario_crea' => $row[9],
	       'notas' => uhtmlentities($row[10]));
?>
    <form action="ver_evento.php" method="POST">
    <div id="form">
    <input type="hidden" style="display: none;" name="id" value=<? echo '"' . $row[0] . '"'; ?> />
    <label class="frm" for="fecha">Fecha de ingreso:</label>
    <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
           value=<? echo '"' . $v['fecha'] . '"'; ?> />
    <label class="frm" for="fecha_evento">Fecha del evento:</label>
    <input class="frm" type="text" readonly="true" name="fecha_evento" id="fecha_evento"
           value=<? echo '"' . $v['fecha_evento'] . '"'; ?> />
    <br />
    <label class="frm" for="tipo">Tipo de evento:</label>
    <?php make_input('tipo', $v, true); ?>
    <label class="frm" for="lugar">Lugar:</label>
    <?php make_input('lugar', $v, true); ?>
    <br />
    <?php if ($v['tipo'] == "Visita al CARE") { ?>
    <label class="frm" for="visitante">Quien visita:</label>
    <?php make_input('visitante', $v, true); ?>
    <label class="frm" for="escuela_segura">Es escuela segura:</label>
    <?php make_input('escuela_segura', $v, true); ?>
    <br />
    <?php } ?>
    <label class="frm" for="usuario_crea">Usuario que captur&oacute;:</label>
    <?php make_input('usuario_crea', $v, true); ?>
    <br />
    <label class="frm" for="notas">Notas:</label>
    <textarea class="frm" id="notas" readonly="true" name="notas"
      ><?php echo $v['notas']; ?></textarea><br />
    <?php
  }
else
  {
    echo '<div class="mensaje">Registro no encontrado.</div>' . "\n";
  }

echo '</div>';

include("footer.html");

if ($con)
  end_connection($con);
?>
