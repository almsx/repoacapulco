<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

include('session.inc');
include('utils.inc');

$noexit = true;
$print = true;

include('header.html');

$result = NULL;
$con = NULL;

$id = $_GET['id'];

if ($id)
  {
    $con = make_connection();

    $query = 'SELECT id, fecha, hora, nombre_de_la_escuela, direccion, colonia, codigo_postal, telefono, id_alarma, lugar_de_entrega, sector_id, persona_que_entrega, persona_que_recibe FROM programa_escuela_segura WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }
?>
<div id="title"><span id="title_center">Ver escuela segura</span></div>
<?php

if ($result)
  {
    $row = mysql_fetch_row($result);
    $sectores = get_poplist_table_array("sectores");

    $v = array('id' => $row[0],
	       'fecha' => $row[1] . ' ' . $row[2],
	       'nombre' => $row[3],
	       'direccion' => $row[4],
	       'colonia' => $row[5],
	       'codigo_postal' => $row[6],
	       'telefono' => $row[7],
	       'cuenta' => $row[8],
	       'lugar_entrega' => $row[9],
	       'sector' => $sectores[$row[10]],
	       'persona_entrega' => $row[11],
	       'persona_recibe' => $row[12]);

?>
    <form action="ver_escuela_segura.php" method="POST">
    <div id="form">
    <input type="hidden" style="display: none;" name="id" value=<? echo '"' . $row[0] . '"'; ?> />
    <label class="frm" for="fecha">Fecha:</label>
    <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
           value=<? echo '"' . $row[1] . ' ' . $row[2] . '"'; ?> />
    <br />
    <label class="frm" for="nombre">Nombre de la escuela:</label>
    <?php make_input('nombre', $v, true); ?>
    <label class="frm" for="direccion">Direcci&oacute;n:</label>
    <?php make_input('direccion', $v, true); ?>
    <br />
    <label class="frm" for="colonia">Colonia:</label>
    <?php make_input('colonia', $v, true); ?>
    <label class="frm" for="codigo_postal">C&oacute;digo postal:</label>
    <?php make_input('codigo_postal', $v, true); ?>
    <br />
    <label class="frm" for="telefono">Tel&eacute;fono:</label>
    <?php make_input('telefono', $v, true); ?>
    <label class="frm" for="cuenta">ID de alarma:</label>
    <?php make_input('cuenta', $v, true); ?>
    <br />
    <label class="frm" for="lugar_entrega">Lugar de la entrega:</label>
    <?php make_input('lugar_entrega', $v, true); ?>
    <label class="frm" for="lugar_entrega">Sector:</label>
    <?php make_input('sector', $v, true); ?>
    <br />
    <label class="frm" for="persona_entrega">Persona que entrega:</label>
    <?php make_input('persona_entrega', $v, true); ?>
    <label class="frm" for="persona_recibe">Persona que recibe:</label>
    <?php make_input('persona_recibe', $v, true); ?>
    <?php
  }
else
  {
    echo '<div class="mensaje">Registro no encontrado.</div>' . "\n";
  }

echo '</div>';

include("footer.html");

if ($con)
  end_connection($con);
?>
