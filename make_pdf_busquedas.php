<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('session.inc');
include('utils.inc');

$fecha_inicio = $_GET['fecha_inicio'];
$fecha_fin = $_GET['fecha_fin'];

$common = "pdf=1&buscar=Buscar&fecha_inicio=$fecha_inicio&fecha_fin=$fecha_fin&ingreso_fecha_inicio=$fecha_inicio&ingreso_fecha_fin=$fecha_fin";
$urls = array();

if ($_GET['sucesos'])
  $urls[] = "buscar_suceso.php?$common";
if ($_GET['sucesos_relevantes'])
  $urls[] = "buscar_suceso_relevante.php?$common";
if ($_GET['beepers'])
  $urls[] = "buscar_beeper.php?$common";
if ($_GET['escuelas_seguras'])
  $urls[] = "buscar_escuela_segura.php?$common";
if ($_GET['alarmas'])
  $urls[] = "buscar_alarmas.php?$common";
if ($_GET['pruebas_alarma'])
  $urls[] = "buscar_pruebas_alarma.php?$common";
if ($_GET['mantenimientos_alarma'])
  $urls[] = "buscar_mantenimientos_alarma.php?$common";
if ($_GET['eventos'])
  $urls[] = "buscar_eventos.php?$common";
if ($_GET['tarjetas'])
  $urls[] = "buscar_tarjeta.php?$common";

make_pdf($urls, '--bottom 3cm --top 4cm', true);
?>
