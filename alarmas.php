<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('session.inc');
include("header.html");
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
      </span>
      <span id="title_center">Solicitud de alarmas</span>
    </div>
<p>
  <a class="button_link"
     href="form_alarmas.php">Nueva solicitud de alarma</a>
</p>
<p>
  <a class="button_link"
     href="buscar_alarmas.php">Buscar solicitudes de alarma</a>
</p>
<?php
include("footer.html");
?>
