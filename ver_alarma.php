<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

include('session.inc');
include('utils.inc');

$noexit = true;
$print = true;

include('header.html');

$result = NULL;
$con = NULL;

$id = ($_GET['id'] ? $_GET['id'] : $_POST['id']);

if ($id)
  {
    $con = make_connection();

    if ($_POST['id'])
      {
	$cquery = 'UPDATE solicitud_de_alarmas SET entregada = ' . $_POST['estado'];
	
	if ($_POST['estado'] == "1"
	    && 0 < strlen(trim($_POST['nombre_entrega']))
	    && 0 < strlen(trim($_POST['nombre_recibe']))
	    && (ereg('^[A-Z]{2}-[0-9]{1,7}$',
		     strtoupper(trim($_POST['cuenta'])))))
	  {
	    $fde = "CURDATE()";
	    $cquery .= ', fecha_de_entrega = ' . $fde .
	      ', nombre_entrega = ' . sqlquote($_POST['nombre_entrega']) .
	      ', nombre_recibe = ' . sqlquote($_POST['nombre_recibe']) .
	      ', id_alarma = ' . sqlquote(strtoupper(trim($_POST['cuenta'])));

	    $cquery .= ' WHERE id = ' . $id . ';';
	    
	    mysql_query($cquery);

	    $avars = "id_alarma, persona, colonia, domicilio, telefono, cap_code";

	    $avalues = sqlquote(strtoupper($_POST['cuenta'])) . ", " .
	      sqlquote($_POST['nombre']) . ", " .
	      sqlquote(ucwords(strtolower($_POST['colonia']))) . ", " .
	      sqlquote(ucwords(strtolower($_POST['domicilio']))) . ", " .
	      sqlquote($_POST['telefono']) . ", " .
	      "0";
	    
	    $aquery = "INSERT INTO alarmas ($avars) values($avalues);";

	    mysql_query($aquery);
	  }
      }

    $query = 'SELECT id, fecha_de_ingreso, fecha_de_solicitud, folio, nombre_solicitante, domicilio, colonia, telefono, id_alarma, fecha_de_entrega, entregada, nombre_entrega, nombre_recibe FROM solicitud_de_alarmas WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }
?>
<div id="title"><span id="title_center">Ver solicitud de alarma</span></div>
<?php

if ($result)
  {
    $row = mysql_fetch_row($result);
    $sectores = get_poplist_table_array("sectores");

    $v = array('id' => $row[0],
	       'fecha_ingreso' => $row[1] ,
	       'fecha_solicitud' => $row[2] ,
	       'folio' => $row[3],
	       'nombre' => $row[4],
	       'domicilio' => $row[5],
	       'colonia' => $row[6],
	       'telefono' => $row[7],
	       'cuenta' => $row[8],
	       'fecha_entrega' => $row[9],
	       'estado' => ($row[10] ? "Entregada" : "Por entregar"),
	       'nombre_entrega' => $row[11],
	       'nombre_recibe' => $row[12]);

?>
    <form action="ver_alarma.php" method="POST">
    <div id="form">
    <input type="hidden" style="display: none;" name="id" value=<? echo '"' . $row[0] . '"'; ?> />
    <label class="frm" for="fecha_ingreso">Fecha de ingreso:</label>
    <input class="frm" type="text" readonly="true" name="fecha_ingreso" id="fecha_ingreso"
           value=<? echo '"' . $row[1] . '"'; ?> />
    <label class="frm" for="fecha_solicitud">Fecha de solicitud:</label>
    <input class="frm" type="text" readonly="true" name="fecha_solicitud" id="fecha_solicitud"
           value=<? echo '"' . $row[2] . '"'; ?> />
    <br />
    <label class="frm" for="folio">Folio:</label>
    <?php make_input('folio', $v, true); ?>
    <br />    
    <label class="frm" for="nombre">Nombre:</label>
    <?php make_input('nombre', $v, true); ?>
    <label class="frm" for="domicilio">Domicilio:</label>
    <?php make_input('domicilio', $v, true); ?>
    <label class="frm" for="telefono">Tel&eacute;fono:</label>
    <?php make_input('telefono', $v, true); ?>
    <label class="frm" for="colonia">Colonia:</label>
    <?php make_input('colonia', $v, true); ?>
    <br />
    <label class="frm" for="nombre_entrega">Quien entrega:</label>
    <?php make_input('nombre_entrega', $v, $row[10]); ?>
    <label class="frm" for="nombre_recibe">Quien recibe:</label>
    <?php make_input('nombre_recibe', $v, $row[10]); ?>
    <br />
    <label class="frm" for="cuenta">ID de alarma:</label>
    <?php make_input('cuenta', $v, $row[10]); ?>
    <br />
    <label class="frm" for="estado">Estado:</label>
    <?php
	 if ($row[10])
	   {
	     make_input('estado', $v, true);
           }
         else
           { ?>
      <select class="frm" id="estado" name="estado">
	<?php
	   $arr = array('estado' => $row[10]);
	   make_option("estado", "Entregado", "1", $arr);
	   make_option("estado", "Por entregar", "0", $arr); 
	?>
      </select><br />
      <?php
	   }
	 if (!$row[10]) { ?>
      <input id="enviar" name="enviar" type="submit" value="Aplicar" />
      <?php }
         else
	   { ?>
      <label class="frm" for="fecha_entrega">Fecha de entrega:</label>
      <?php make_input('fecha_entrega', $v, true); 
	   }?> 
      <br />
    <?php
  }
else
  {
    echo '<div class="mensaje">Registro no encontrado.</div>' . "\n";
  }

echo '</div>';

include("footer.html");

if ($con)
  end_connection($con);
?>
