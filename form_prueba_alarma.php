<?php
$allow = array(1 => true, 2 => true, 3 => true);

$perfiles = array(1 => "Administrador", "Supervisor", "Operador");

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    if ((1 > strlen(trim($_POST['cuenta'])))
	|| !ereg('^[A-Z]{2}-[0-9]{1,7}$',
		 strtoupper(trim($_POST['cuenta']))))
      $errors[] = "ID de alarma";
    if (1 > strlen(trim($_POST['persona_prueba'])))
      $errors[] = "Con quien se realiz&oacute; la prueba";

    $valid = count($errors) == 0;

    if ($valid)
      {
	$vars = "id_alarma, tipo_de_domicilio, persona_prueba, resultado, notas";

	$values = sqlquote($_POST['cuenta']) . ", " .
	  $_POST['tipo_domicilio'] . ", " .
	  sqlquote($_POST['persona_prueba']) . ", " .
	  $_POST['resultado'] . ", " .
	  sqlquote($_POST['notas']);
	
	$query = "INSERT INTO prueba_de_alarmas (fecha, hora, $vars) " .
	  "values(CURDATE(), CURTIME(), $values);";

	mysql_query($query);
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=prueba_alarmas.php" />';
  }

include('header.html');

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="prueba_alarmas.php">Pruebas de alarma</a>: 
      </span>
      <span id="title_center">Nueva prueba de alarma</span>
    </div>
    <form action="form_prueba_alarma.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>El contenido de los siguientes campos no es v&aacute;lido:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
      <input type="hidden" style="display: none;" name="commit" value="1" />
      <label class="frm" for="fecha">Fecha:</label>
      <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
	     value=<? echo '"' . date("d/m/Y H:i") . '"'; ?> />
      <br />
      <label class="frm" for="cuenta">ID de alarma:</label>
      <?php make_input('cuenta'); ?>
      <label class="frm" for="tipo_domicilio">Tipo de domicilio:</label>
	<select class="frm" id="tipo_domicilio" name="tipo_domicilio">
	<?php
	   make_option("tipo_domicilio", "Casa", "0"); 
	   make_option("tipo_domicilio", "Comercio", "1"); 
	   make_option("tipo_domicilio", "Escuela Segura", "2"); 
	?>
	</select>
      <br />
      <label class="frm" for="persona_prueba">Con quien se hizo la prueba:</label>
      <?php make_input('persona_prueba'); ?>
      <label class="frm" for="resultado">Resultado:</label>
	<select class="frm" id="resultado" name="resultado">
	<?php
	   make_option("resultado", "Exitosa", "1"); 
	   make_option("resultado", "Fallida", "0"); 
	?>
	</select>
      <br />
      <label class="frm" for="notas">Notas:</label>
      <textarea class="frm" id="notas" name="notas"
	><?php echo uhtmlentities($_POST['notas']); ?></textarea><br />
      <input id="enviar" name="enviar" type="submit" value="Enviar" />
      <br />
      </div>
    </form>

<?php
  }

include("footer.html");

end_connection($con);
?>
