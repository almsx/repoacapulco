<?php
$allow = array(1 => true, 2 => true, 3 => true);

$perfiles = array(1 => "Administrador", "Supervisor", "Operador");

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    if ($_POST['tipo_evento_care'] != "0")
      {
	if (1 > strlen(trim($_POST['tipo_evento'])))
	  $errors[] = "Tipo de evento (Otro)";
	if (1 > strlen(trim($_POST['lugar'])))
	  $errors[] = "Lugar del evento (Otro)";
      }
    else
      {
	if (1 > strlen(trim($_POST['visitante'])))
	  $errors[] = "Quien visita";
      }

    $valid = count($errors) == 0;

    if ($valid)
      {
	$vars = "fecha_evento, hora_evento, tipo, lugar, visitante, escuela_segura, usuario_crea, notas";

	$values = sqlquote($_POST['fecha_evento']) . ", " .
	  sqlquote($_POST['hora_evento_h'] . ":" . $_POST['hora_evento_m'] . ':00') . ", " .
	  ($_POST['tipo_evento_care'] == "0" ? '"Visita al CARE"' : sqlquote($_POST['tipo_evento'])) . ", " .
	  ($_POST['tipo_evento_care'] == "0" ? '"CARE"' : sqlquote($_POST['lugar'])) . ", " .
	  ($_POST['tipo_evento_care'] != "0" ? '""' : sqlquote($_POST['visitante'])) . ", " .
	  ($_POST['tipo_evento_care'] != "0" ? "0" : $_POST['escuela_segura']) . ", " .
	  sqlquote($_SESSION['login_name']) . ", " .
	  sqlquote($_POST['notas']);
	
	$query = "INSERT INTO eventos (fecha, hora, $vars) " .
	  "values(CURDATE(), CURTIME(), $values);";

	mysql_query($query);
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=eventos.php" />';
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include('header.html');

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="eventos.php">Eventos</a>: 
      </span>
      <span id="title_center">Ingresar evento</span>
    </div>
    <form action="form_evento.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>El contenido de los siguientes campos no es v&aacute;lido:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
      <input type="hidden" style="display: none;" name="commit" value="1" />
      <label class="frm" for="fecha">Fecha:</label>
      <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
	     value=<? echo '"' . date("d/m/Y H:i") . '"'; ?> />
      <br />
      <label class="frm" for="fecha_evento">Fecha del evento:</label>
      <script>DateInput('fecha_evento', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="hora_evento_h">Hora del evento:</label>
      <?php make_number_select("hora_evento_h", 0, 23, 0); ?>
      <?php make_number_select("hora_evento_m", 0, 59, 0); ?>
      <br />
      <label class="frm" for="tipo_evento_care">Tipo de evento:</label>
      <select class="frm" id="tipo_evento_care" name="tipo_evento_care">
      <?php
	   make_option("tipo_evento_care", "Visita al CARE", "0"); 
	   make_option("tipo_evento_care", "Otro (especificar)", "1"); 
      ?>
      </select>
      <br />
      <label class="frm" for="tipo_evento">Tipo de evento (Otro):</label>
      <?php make_input('tipo_evento'); ?>
      <label class="frm" for="lugar">Lugar del evento (Otro):</label>
      <?php make_input('lugar'); ?>
      <br />
      <label class="frm" for="visitante">Quien visita:</label>
      <?php make_input('visitante'); ?>
      <label class="frm" for="escuela_segura">Es escuela segura:</label>
      <select class="frm" id="escuela_segura" name="escuela_segura">
      <?php
	   make_option("escuela_segura", "No", "0"); 
	   make_option("escuela_segura", "Si", "1"); 
      ?>
      </select>
      <br />
      <label class="frm" for="notas">Notas:</label>
      <textarea class="frm" id="notas" name="notas"
	><?php echo uhtmlentities($_POST['notas']); ?></textarea>
      <br />
      <input id="enviar" name="enviar" type="submit" value="Enviar" />
      <br />
      </div>
    </form>

<?php
  }

include("footer.html");

end_connection($con);
?>
