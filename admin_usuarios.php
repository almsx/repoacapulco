<?php
$allow = array(1 => true);

include('session.inc');
include("header.html");
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="admin.php">Administrador</a>:
      </span>
      <span id="title_center">Usuarios</span>
    </div>
<p>
  <a class="button_link"
     href="nuevo_usuario.php">Nuevo usuario</a>
</p>
<p>
  <a class="button_link"
     href="borrar_usuario.php">Borrar usuario</a>
</p>
<p>
  <a class="button_link"
     href="mostrar_usuarios.php">Mostrar usuarios</a>
</p>
<?php
include("footer.html");
?>
