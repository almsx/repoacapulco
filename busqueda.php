<?php
$allow = array(1 => true, 2 => true, 3 => true);
if (!$_GET['pdf'])
include('session.inc');
include("utils.inc");

/*
    Realiza una conexion al DBMS MySql y selecciona una BD.

*/
 
$conexio;
function conectar_bd()
{
    global $conexion;
    //Definir datos de conexion con el servidor MySQL
    $elUsr = "intralte_mapas";
    $elPw  = "hola123";
    $elServer ="localhost";
    $laBd = "intralte_mapas";
 
    $conexio = mysql_connect($elServer, $elUsr , $elPw) or die (mysql_error());
     
    mysql_select_db($laBd, $conexio ) or die (mysql_error());
 

    if ($_GET['fecha_inicio'] && $_GET['fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha >= ' . sqlquote($_GET['fecha']);
    if ($_GET['fecha_fin'] && $_GET['fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha <= ' . sqlquote($_GET['fecha_fin']);
	  
	}  

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';
  include("header.html");
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">inicio</a>: 
        <a class="title_link" href="localizacion.php"></a>
      </span>
      <span id="title_center">Busqueda</span>
    </div>
    <style>
        #mapa{
            width: 1300px;
            height: 600px;
            float:center;
            background: #f4f1e8;
        }
        #infor{
            width: 400px;
            height: 400px;
            float:left;
			background:#f4f1e8;
        }
    </style>
<!--IMPORTANTE RESPETAR EL ORDEN -->
<!--ESTILOS DE BOOSTRAP -->
<link href="api/css/bootstrap.min.css" rel="stylesheet" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js" ></script>
<!--ARCHIVOS JAVASCRIPT DE BOOTSTRAP -->
<script type="text/javascript" src="api/js/bootstrap.min.js" ></script>


<script>
    //VARIABLES 
	
		//declaras fuera del ready de jquery
    var nuevos_marcadores = [];
    var marcadores_bd= [];
    var mapa = null; //VARIABLE GENERAL PARA EL MAPA
    //FUNCION PARA QUITAR MARCADORES DE MAPA
    function limpiar_marcadores(lista)
    {
        for(i in lista)
        {
            //QUITAR MARCADOR DEL MAPA
            lista[i].setMap(null);
        }
    }
    $(document).on("ready", function(){
        
        //VARIABLE DE FORMULARIO
        var formulario = $("#formulario");
        
        var punto = new google.maps.LatLng(16.843976,-99.873619);
        var config = {
            zoom:13,
            center:punto,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        mapa = new google.maps.Map( $("#mapa")[0], config );

        google.maps.event.addListener(mapa, "click", function(event){
           var coordenadas = event.latLng.toString();
           
           coordenadas = coordenadas.replace("(", "");
           coordenadas = coordenadas.replace(")", "");
           
           var lista = coordenadas.split(",");
           
           var direccion = new google.maps.LatLng(lista[0], lista[1]);
           //PASAR LA INFORMACI?N AL FORMULARIO
           formulario.find("input[name='titulo']").focus();
		   formulario.find("input[name='fecha']").focus();
           formulario.find("input[name='cx']").val(lista[0]);
           formulario.find("input[name='cy']").val(lista[1]);
           
           
           var marcador = new google.maps.Marker({
               //titulo:prompt("Titulo del marcador?"),
               position:direccion,
               map: mapa, 
               animation:google.maps.Animation.DROP,
               draggable:false
           });
           //ALMACENAR UN MARCADOR EN EL ARRAY nuevos_marcadores
           nuevos_marcadores.push(marcador);
           
           google.maps.event.addListener(marcador, "click", function(){

           });
           
           //BORRAR MARCADORES NUEVOS
           limpiar_marcadores(nuevos_marcadores);
           marcador.setMap(mapa);
        });
        $("#btn_grabar").on("click", function(){
            //INSTANCIAR EL FORMULARIO
            var f = $("#formulario");
            
            //VALIDAR CAMPO TITULO
            if(f.find("input[name='titulo']").val().trim()=="")
            {
                alert("Falta t?tulo");
                return false;
            }
			//VALIDAR CAMPO FECHA
            if(f.find("input[name='fecha']").val().trim()=="")
            {
                alert("Falta fecha");
                return false;
            }
            //VALIDAR CAMPO CX
            if(f.find("input[name='cx']").val().trim()=="")
            {
                alert("Falta Coordenada X");
                return false;
            }
            //VALIDAR CAMPO CY
            if(f.find("input[name='cy']").val().trim()=="")
            {
                alert("Falta Coordenada Y");
                return false;
            }
            //FIN VALIDACIONES
            
            if(f.hasClass("busy"))
            {
                //Cuando se haga clic en el boton grabar
                //se le marcar? con una clase 'busy' indicando
                //que ya se ha presionado, y no permitir que se
                //realiCe la misma operaci?n hasta que esta termine
                //SI TIENE LA CLASE BUSY, YA NO HARA NADA
                return false;
            }
            //SI NO TIENE LA CLASE BUSY, SE LA PONDREMOS AHORA
            f.addClass("busy");
            //Y CUANDO QUITAR LA CLASE BUSY?
            //CUANDO SE TERMINE DE PROCESAR ESTA SOLICITUD
            //ES DECIR EN EL EVENTO COMPLETE
            
            var loader_grabar = $("#loader_grabar");
           $.ajax({
               type:"POST",
               url:"api/iajax.php",
               dataType:"JSON",
               data:f.serialize()+"&tipo=grabar",
               success:function(data){
                   if(data.estado=="ok")
                    {
                        loader_grabar.removeClass("label-warning").addClass("label-success")
                        .text("Grabado OK").delay(4000).slideUp();
                        listar();
                    }
                    else
                    {
                        alert(data.mensaje);
                    }
                   
                   
               },
               beforeSend:function(){
                   //Notificar al usuario mientras que se procesa su solicitud
                   loader_grabar.removeClass("label-success").addClass("label label-warning")
                   .text("Procesando...").slideDown();
               },
               complete:function(){
                   //QUITAR LA CLASE BUSY
                   f.removeClass("busy");
                   f[0].reset();
                   //[0] jquery trabaja con array de elementos javascript no
                   //asi que se debe especificar cual elemento se har? reset
                   //capricho de javascript
                   //AHORA PERMITIR? OTRA VEZ QUE SE REALICE LA ACCION
                   //Notificar que se ha terminado de procesar
                   
               }
           });
           return false;
        });
        //BORRAR
        $("#btn_borrar").on("click", function(){
            //CONFIRMAR ACCION DEL USUARIO
            if(confirm("Est� seguro?")==false)
            {
                //NO HARA NADA
                return;
            }
            var formulario_edicion = $("#formulario_edicion");
            $.ajax({
                type:"POST",
                url:"api/iajax.php",
                data:formulario_edicion.serialize()+"&tipo=borrar",
                dataType:"JSON",
                success:function(data){
                    //SABER CUANDO SE BORR� CORRECTAMENTE
                    if(data.estado=="ok")
                    {
                        //MOSTRAR EL MENSAJE
                        alert(data.mensaje);
                        //BORRAR MARCADORES NUEVOS EN CASO DE QUE HUBIESEN
                        limpiar_marcadores(nuevos_marcadores);
                        //LIMPIAR EL FORMULARIO
                        formulario_edicion[0].reset();
                        //LISTAR OTRA VEZ LOS MARCADORES
                        listar();
                    }
                    else
                    {
                        //ERROR AL BORRAR
                        alert(data.mensaje);
                    }
                    
                },
                beforeSend:function(){
                    
                },
                complete:function(){
                    
                }
            });
        });
        
        //CARGAR PUNTOS AL TERMINAR DE CARGAR LA P?GINA
        listar();//FUNCIONA, AHORA A GRAFICAR LOS PUNTOS EN EL MAPA
    });
    //FUERA DE READY DE JQUERY
    //FUNCTION PARA RECUPERAR PUNTOS DE LA BD
    function listar()
    {
        //ANTES DE LISTAR MARCADORES
        //SE DEBEN QUITAR LOS ANTERIORES DEL MAPA
       limpiar_marcadores(marcadores_bd);
       var formulario_edicion = $("#formulario_edicion");
       $.ajax({
               type:"POST",
               url:"<!--//api/iajax.php-->",
               dataType:"JSON",
               data:"&tipo=listar",
               success:function(data){
                   if(data.estado=="ok")
                    {
                        //alert("Hay puntos en la BD");
                        $.each(data.mensaje, function(i, item){
                            //OBTENER LAS COORDENADAS DEL PUNTO
                            var posi = new google.maps.LatLng(item.cx, item.cy);//bien
                            //CARGAR LAS PROPIEDADES AL MARCADOR
                            var marca = new google.maps.Marker({
                                idMarcador:item.IdPunto,
                                position:posi,
                                titulo: item.Titulo,
								fecha:  item.fecha,
                                cx:item.cx,//esas coordenadas vienen de la BD
                                cy:item.cy//esas coordenadas vienen de la BD
                            });
                            //AGREGAR EVENTO CLICK AL MARCADOR
                            //MARCADORES QUE VIENEN DE LA BASE DE DATOS
                            google.maps.event.addListener(marca, "click", function(){
                               //ENTRAR EN EL SEGUNDO COLAPSIBLE
                               //Y OCULTAR EL PRIMERO
                               $("#collapseTwo").collapse("show");
                               $("#collapseOne").collapse("hide");
                               //VER DOCUMENTACI�N DE BOOTSTRAP :)
                               
                               //AHORA PASAR LA INFORMACI�N DEL MARCADOR
                               //AL FORMUALARIO
                               formulario_edicion.find("input[name='id']").val(marca.idMarcador);
                               formulario_edicion.find("input[name='titulo']").val(marca.titulo).focus();
							   formulario_edicion.find("input[name='fecha']").val(marca.fecha).focus();
                               formulario_edicion.find("input[name='cx']").val(marca.cx);
                               formulario_edicion.find("input[name='cy']").val(marca.cy);
                               
                            });
                            //AGREGAR EL MARCADOR A LA VARIABLE MARCADORES_BD
                            marcadores_bd.push(marca);
                            //UBICAR EL MARCADOR EN EL MAPA
                            marca.setMap(mapa);
                        });
                    }
                else
                    {
                        alert("NO hay puntos en la BD");
                    }
               },
               beforeSend:function(){
                   
               },
               complete:function(){
                   
               }
           });
    }
    //PLANTILLA AJAX
    
</script>
</head>
<body>
    <div id="mapa">
        <h2>Aqui ir� el mapa!</h2>
    </div>

	<form action="busqueda.php" method="GET">
      <div id="form">
      <label class="frm" for="fecha_inicio">Fecha inicial:</label>
      <script>DateInput('fecha_inicio', true, 'YYYY-MM-DD', '2013-12-05')</script>
      <label class="frm" for="fecha_fin">Fecha final:</label>
      <script>DateInput('fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
	      <input id="buscar" name="buscar" type="submit" value="Buscar" />  
</form>
<?php
include("footer.html");
?>