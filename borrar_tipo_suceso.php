<?php
$allow = array(1 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

if (1 == $_POST["commit"])
  { 
    $id = sqlquote($_POST['tipo_suceso']);
    $query = "UPDATE tipos_de_suceso SET borrado = 1 WHERE id = $id;";

    mysql_query($query);
  }

$redirect = $_POST['borrar'];
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=admin_tipos_suceso.php" />';
  }

include("header.html");

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="admin.php">Administrador</a>: 
        <a class="title_link" href="admin_tipos_suceso.php">Tipos de Suceso</a>: 
      </span>
      <span id="title_center">Borrar</span>
    </div>
    <form action="borrar_tipo_suceso.php" method="POST">
      <div id="form">
        <input type="hidden" style="display: none;" name="commit" value="1" />
	<label class="frm" for="tipo_suceso">Tipo de suceso:</label>
	<?php make_select('tipo_suceso', 'tipos_de_suceso'); ?>
	<br />
	<br />
	<br />
	<input id="borrar" name="borrar" type="submit" value="Borrar" />
	<br />
      </div>
    </form>

<?php
  }
include("footer.html");
end_connection($con);
?>
