<?php
$allow = array(1 => true, 2 => true, 4 => true);

include('session.inc');

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="reportes.php">Reportes</a>: 
      </span>
      <span id="title_center">Reporte de b&uacute;squedas</span>
    </div>
    <form target="_blank" action="make_pdf_busquedas.php" method="GET">
      <div id="form">
      <label class="frm" for="fecha_inicio">Fecha inicial:</label>
      <script>DateInput('fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="fecha_fin">Fecha final:</label>
      <script>DateInput('fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="sucesos">Sucesos:</label>
      <input class="cb" type="checkbox" id="sucesos"
	     name="sucesos" value="1" checked="true" />
      <label class="frm" for="sucesos_relevantes">Sucesos relevantes:</label>
      <input class="cb" type="checkbox" id="sucesos_relevantes"
	     name="sucesos_relevantes" value="1" checked="true" />
      <label class="frm" for="beepers">Entregas de beepers:</label>
      <input class="cb" type="checkbox" id="beepers"
	     name="beepers" value="1" checked="true" />
      <br />
      <label class="frm" for="escuelas_seguras">Escuelas seguras:</label>
      <input class="cb" type="checkbox" id="escuelas_seguras"
	     name="escuelas_seguras" value="1" checked="true" />
      <label class="frm" for="alarmas">Solicitudes de alarma:</label>
      <input class="cb" type="checkbox" id="alarmas"
	     name="alarmas" value="1" checked="true" />
      <label class="frm" for="pruebas_alarma">Pruebas alarma:</label>
      <input class="cb" type="checkbox" id="pruebas_alarma"
	     name="pruebas_alarma" value="1" checked="true" />
      <br />
      <label class="frm" for="mantenimientos_alarma">Mantenimientos de alarma:</label>
      <input class="cb" type="checkbox" id="mantenimientos_alarma"
	     name="mantenimientos_alarma" value="1" checked="true" />
      <label class="frm" for="eventos">Eventos:</label>
      <input class="cb" type="checkbox" id="eventos"
	     name="eventos" value="1" checked="true" />
      <label class="frm" for="tarjetas">Tarjetas informativas:</label>
      <input class="cb" type="checkbox" id="tarjetas"
	     name="tarjetas" value="1" checked="true" />
      <br />
      <br />
      <input id="ver" name="ver" type="submit" value="Ver" />
      <br />
      </div>
    </form>
<?php

include("footer.html");
?>
