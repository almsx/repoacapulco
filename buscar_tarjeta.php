<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

if (!$_GET['pdf'])
  include('session.inc');
include("utils.inc");

$con = make_connection();

$results = NULL;

if ($_GET['buscar'])
  {
    $filters = array();
    $filter_string = "";
    $query = NULL;

    if ($_GET['fecha_inicio'] && $_GET['fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha >= ' . sqlquote($_GET['fecha_inicio']);
    if ($_GET['fecha_fin'] && $_GET['fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha <= ' . sqlquote($_GET['fecha_fin']);
    if (0 < strlen(trim($_GET['destinatario'])))
      $filters[] = 'destinatario LIKE ' . sqlquote('%' . ucwords(strtolower($_GET['destinatario'])) . '%');
    if (0 < strlen(trim($_GET['remitente'])))
      $filters[] = 'remitente LIKE ' . sqlquote('%' . ucwords(strtolower($_GET['remitente'])) . '%');
    if (0 < strlen(trim($_GET['folio'])))
      $filters[] = 'folio LIKE ' .  sqlquote('%' . strtoupper($_GET['folio']) . '%');

    if (count($filters) > 0)
      $filterstr .= "WHERE " . implode(" AND ", $filters);

    $query = "SELECT id, folio, fecha, destinatario, remitente FROM tarjetas_informativas $filterstr;";

    $results = mysql_query($query);
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");

?>
    <?php if (!$_GET['pdf']) { ?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="tarjetas_informativas.php">Tarjetas informativas</a>:
      </span>
      <span id="title_center">B&uacute;squeda</span>
    </div>
    <form action="buscar_tarjeta.php" method="GET">
      <div id="form">
      <label class="frm" for="fecha_inicio">Fecha inicial:</label>
      <script>DateInput('fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="fecha_fin">Fecha final:</label>
      <script>DateInput('fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="destinatario">Destinatario:</label>
      <?php make_input('destinatario', $_GET); ?>
      <label class="frm" for="remitente">Remitente:</label>
      <?php make_input('remitente', $_GET); ?>
      <br />
      <label class="frm" for="folio">Folio:</label>
      <?php make_input('folio', $_GET); ?>
      <br />
      <input id="buscar" name="buscar" type="submit" value="Buscar" />
      <br />
      </div>
    </form>
    <?php
      if ($_GET['buscar'])
	{
	  $url = "buscar_tarjeta.php?" . urlencode(http_build_query($_GET));
	  echo "<a target=\"_blank\" href=\"make_pdf.php?url=$url\">Exportar a PDF</a>";
	}
      }
    ?>
    <?php if ($_GET['pdf']) { echo '<center><h3>Tarjetas informativas</h3></center>' . "\n"; } ?>
    <hr />
<?php

if ($results)
  {
    $headers = array("ID", "Folio", "Fecha", "Destinatario", "Remitente");

    echo '<div id="search_results">' . "\n";
    echo '<table id="search_results">' . "\n";
    echo '<tr class="header">';
    
    foreach ($headers as $hd)
      echo '<th class="header">' . $hd . '</th>';

    $medios_de_recepcion = get_poplist_table_array("medios_de_recepcion");
    $tipos_de_suceso = get_poplist_table_array("tipos_de_suceso");

    $num = 0;
    while ($row = mysql_fetch_row($results))
      {
	echo '<tr onClick="window.open(\'ver_tarjeta.php?id=' .
	  $row[0] . '\', \'Suceso\', \'width=730,height=500,status=0,toolbar=0\');" onmouseover="this.style.backgroundColor = \'#ffffff\';" onmouseout="this.style.backgroundColor = \'#f5f1e8\';" class="result">';

	echo '<td class="result_cell">' . $row[0] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[1] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[2] . '</td>' . "\n";
	echo '<td class="result_cell">' . uhtmlentities($row[3]) . '</td>' . "\n";
	echo '<td class="result_cell">' . uhtmlentities($row[4]) . '</td>' . "\n";
	echo '</tr>' . "\n";
	$num += 1;
      }
    echo '</table>' . "\n";
    echo "<br /><p>Total de registros: $num</p>";
    echo '</div>' . "\n";
  }

include("footer.html");

end_connection($con);
?>
