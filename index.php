<?php

session_start();

include('utils.inc');
include("header.html");

$logged = false;

if ($_GET['logout'] == "1")
  {
    $_SESSION['login_id'] = NULL;
    session_destroy();
  }
else if (strlen($_POST['username']) > 0 && strlen($_POST['password']) > 0)
  {
    $con = make_connection();
    $query = 'SELECT id, nombre, perfil, nombre_persona, apellidos FROM usuarios WHERE nombre = ' .
      sqlquote($_POST['username']) . " AND pass = " .
      sqlquote(md5($_POST['password'])) . ';';

    $results = mysql_query($query);
    
    if ($results)
      { 
	$row = mysql_fetch_row($results);
	if ($row)
	  {
	    $_SESSION['login_id'] = $row[0];
	    $_SESSION['login_name'] = $row[1];
	    $_SESSION['profile'] = $row[2];
	    $_SESSION['user_realname'] = $row[3] . ' ' . $row[4];
	  }
      }
    end_connection($con);
  }

if ($_SESSION['login_id'] != NULL)
  $logged = true;

?>
<div id="title"><span id="title_center">Inicio</span></div>
<?php
  if (!$logged)
    {
?>
<form action="index.php" method="POST">
  <div id="form">
<?php
if ($_POST['enviar'] != NULL)
  {
    echo '<div class="errores">';
    echo '<p>El nombre de usuario y la contrase&ntilde;a no coinciden.</p>';
    echo '</div>';
  }
?>
    <label class="frm" for="username">Nombre de usuario:</label>
    <?php make_input('username'); ?>
    <label class="frm" for="password">Contrase&ntilde;a:</label>
    <input class="frm" type="password" id="password" name="password" value="" />
    <br />
    <input id="enviar" name="enviar" type="submit" value="Entrar" />
    <br />
  </div>
</form>
<?php
    }
  else
    {
      if ($_SESSION['profile'] == "1")
	echo '<a class="button_link" href="admin.php">Panel de administrador</a>' . "\n";
?>
<?php
  if ($_SESSION['profile'] == "1" || $_SESSION['profile'] == "2" || $_SESSION['profile'] == "3")
    {
?>
<p>
  <a class="button_link"
     href="sucesos.php">Sucesos</a>
</p>
<p>
  <a class="button_link"
     href="criminalidad.php">Criminalidad</a>
</p>
<p>
 <!-- <a class="button_link"
     href="escuela_segura.php">Programa Escuela Segura</a>-->
</p>
<p>
  <!--<a class="button_link"
     href="alarmas.php">Solicitud de alarmas</a>-->
</p>
<p>
  <!--<a class="button_link"
     href="prueba_alarmas.php">Pruebas de alarma</a>-->
</p>
<p>
  <a class="button_link"
     href="semaforos.php">Semaforos</a>
</p>
<p>
  <!--<a class="button_link"
     href="eventos.php">Eventos</a>-->
</p>
<p>
  <!--<a class="button_link"
     href="http://www.comseg.com.mx/mensajes/wfDMessenger.aspx"
     target="_blank">Mandar mensaje a beeper</a>-->
</p>
<p>
   <!--<a class="button_link"
      href="tarjetas_informativas.php">Tarjetas informativas</a>-->
</p>
<?php } ?>
<p>
<?php
   if ($_SESSION['profile'] == "1" || $_SESSION['profile'] == "2" || $_SESSION['profile'] == "4")
     echo '<a class="button_link" href="reportes.php">Reportes</a>' . "\n";
	 echo '<p><!--break_space--></p>'."\n";
	  echo '<a class="button_link" href="localizacion.php">Localizaci&#243;n</a>' . "\n";
?>
</p>
<?php
    }

include("footer.html");
?>
