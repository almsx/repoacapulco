<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    if (1 > strlen(trim($_POST['domicilio'])))
      $errors[] = "Domicilio";
    $tel = ereg_replace("[ -]", "", trim($_POST['telefono']));
    if (8 > strlen($tel) || !is_numeric($tel))
      $errors[] = "Tel&eacute;fono";
    $folio = ereg_replace("^0*", "", trim($_POST['folio']));
    if (!ereg("^[1-9][0-9]*/[0-9][0-9][0-9]$", $folio))
      $errors[] = "Folio";
    if (1 > strlen($_POST['nombre']))
      $errors[] = "Quien hizo la solicitud";
    if ($_POST['estado'] == "1")
      {
	if (1 > strlen($_POST['nombre_recibe']))
	  $errors[] = "Quien  recibe";
	if (1 > strlen($_POST['nombre_entrega']))
	  $errors[] = "Quien entrega";
	if ((1 > strlen(trim($_POST['cuenta'])))
	    || !ereg('^[A-Z]{2}-[0-9]{1,7}$',
		     strtoupper(trim($_POST['cuenta']))))
	  $errors[] = "ID de alarma";
      }

    $valid = count($errors) == 0;

    if ($valid)
      {
	$vars = "folio, nombre_solicitante, domicilio, colonia,  telefono, fecha_de_solicitud, id_alarma, fecha_de_entrega, entregada, nombre_recibe, nombre_entrega";
	
	$values = sqlquote($folio) . ", " .
	  sqlquote($_POST['nombre']) . ", " .
	  sqlquote(ucwords(strtolower($_POST['domicilio']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['colonia']))) . ", " .
	  sqlquote($tel) . ", " .
	  sqlquote($_POST['fecha_solicitud']) . ", " .
	  ($_POST['estado'] == "1" ? sqlquote(strtoupper($_POST['cuenta'])) : sqlquote("")) . ", " .
	  ($_POST['estado'] == "1" ? "CURDATE()" : "0") . ", " .
	  ($_POST['estado'] == "1" ? "true" : "false") . ", " .
	  ($_POST['estado'] == "1" ? sqlquote(ucwords(strtolower($_POST['nombre_recibe']))) : sqlquote(""))  . ", " .
	  ($_POST['estado'] == "1" ? sqlquote(ucwords(strtolower($_POST['nombre_entrega']))) : sqlquote(""));
	
	$query = "INSERT INTO solicitud_de_alarmas (fecha_de_ingreso, $vars) " .
	  "values(CURDATE(), $values);";

	mysql_query($query);
	
	if ($_POST['estado'] == "1")
	  {
	    $avars = "id_alarma, persona, colonia, domicilio, telefono, cap_code";

	    $avalues = sqlquote(strtoupper($_POST['cuenta'])) . ", " .
	      sqlquote($_POST['nombre_recibe']) . ", " .
	      sqlquote(ucwords(strtolower($_POST['colonia']))) . ", " .
	      sqlquote(ucwords(strtolower($_POST['domicilio']))) . ", " .
	      sqlquote($tel) . ", " .
	      "0";
	    
	    $aquery = "INSERT INTO alarmas ($avars) values($avalues);";

	    mysql_query($aquery);
	  }
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=alarmas.php" />';
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include('header.html');

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="alarmas.php">Solicitudes de alarma</a>: 
      </span>
      <span id="title_center">Solicitud de alarma</span>
    </div>
    <form action="form_alarmas.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>El contenido de los siguientes campos no es v&aacute;lido:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
      <input type="hidden" style="display: none;" name="commit" value="1" />
      <input type="hidden" style="display: none;" name="type"
             <?php echo 'value="' . $tipo_form . '"'; ?> />
      <label class="frm" for="fecha">Fecha de ingreso:</label>
      <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
	     value=<? echo '"' . date("d/m/Y") . '"'; ?> />
      <br />
      <label class="frm" for="fecha_solicitud">Fecha de solicitud:</label>
      <script>DateInput('fecha_solicitud', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="folio">Folio:</label>
      <?php make_input('folio'); ?>
      <br />
      <label class="frm" for="nombre">Quien hizo la solicitud:</label>
      <?php make_input('nombre'); ?>
      <br />
      <label class="frm" for="domicilio">Domicilio:</label>
      <?php make_input('domicilio'); ?>
      <label class="frm" for="colonia">Colonia:</label>
      <?php make_input('colonia'); ?>
      <br />
      <label class="frm" for="telefono">Tel&eacute;fono:</label>
      <?php make_input('telefono'); ?>
      <label class="frm" for="cuenta">ID de alarma:</label>
      <?php make_input('cuenta'); ?>
      <br />
      <label class="frm" for="nombre_entrega">Quien entrega:</label>
      <?php make_input('nombre_entrega'); ?>
      <label class="frm" for="nombre_recibe">Quien recibe:</label>
      <?php make_input('nombre_recibe'); ?>
      <br />
      <label class="frm" for="estado">Estado:</label>
      <select class="frm" id="estado" name="estado">
	<?php
	   make_option("estado", "Por entregar", "0");
	   make_option("estado", "Entregada", "1"); 
	?>
      </select><br />
      <input id="enviar" name="enviar" type="submit" value="Enviar" />
      <br />
      </div>
    </form>

<?php
  }

include("footer.html");

end_connection($con);
?>
