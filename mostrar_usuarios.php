<?php
$allow = array(1 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

include("header.html");
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="admin.php">Administrador</a>: 
        <a class="title_link" href="admin_usuarios.php">Usuarios</a>: 
      </span>
      <span id="title_center">Mostrar</span>
    </div>
<?php

$results = mysql_query("SELECT id, nombre, perfil, nombre_persona, apellidos, telefono, direccion, dependencia FROM usuarios ORDER BY perfil ASC;");

if ($results)
  {
    $headers = array("ID", "Usuario", "Perfil", "Nombre", "Tel&eacute;fono", "Direcci&oacute;n", "Dependencia");
    $perfiles = array( 1 => "Administrador", "Supervisor", "Operador", "Visitante");

    echo '<div id="search_results">' . "\n";
    echo '<table id="search_results">' . "\n";
    echo '<tr class="header">';
    
    foreach ($headers as $hd)
      echo '<th class="header">' . $hd . '</th>';

    while ($row = mysql_fetch_row($results))
      {
	echo '<tr class="result">';

	echo '<td class="result_cell_np">' . $row[0] . '</td>' . "\n";
	echo '<td class="result_cell_np">' . uhtmlentities($row[1]) . '</td>' . "\n";
	echo '<td class="result_cell_np">' . $perfiles[$row[2]] . '</td>' . "\n";
	echo '<td class="result_cell_np">' . uhtmlentities($row[3] . ' ' . $row[4]) . '</td>' . "\n";
	echo '<td class="result_cell_np">' . $row[5] . '</td>' . "\n";
	echo '<td class="result_cell_np">' . uhtmlentities($row[6]) . '</td>' . "\n";
	echo '<td class="result_cell_np">' . uhtmlentities($row[7]) . '</td>' . "\n";
	echo '</tr>' . "\n";
      }
    echo '</table>' . "\n";
    echo '</div>' . "\n";
  }

include("footer.html");
?>
