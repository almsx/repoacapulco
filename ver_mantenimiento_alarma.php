<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

include('session.inc');
include('utils.inc');

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

$noexit = true;
$print = true;

include('header.html');

$result = NULL;
$con = NULL;

$id = ($_GET['id'] ? $_GET['id'] : $_POST['id']);

if ($id)
  {
    $con = make_connection();

    if ($_POST['id'])
      {
	$cquery = 'UPDATE mantenimiento_de_alarmas SET estado = ' . $_POST['estado'];
	
	if ($_POST['estado'] != "0"
	    && 0 < strlen(trim($_POST['persona_recibe'])))
	  {
	    $cquery .= ', fecha_devolucion = ' . sqlquote($_POST['fecha_devolucion']) .
	      ', hora_devolucion = ' . sqlquote($_POST['hora_devolucion_h'] . ':' . $_POST['hora_devolucion_m'] . ':00') .
	      ', persona_recibe = ' . sqlquote($_POST['persona_recibe']);

	    if (($_POST['estado'] == "2"
		 && ereg('^[A-Z]{2}-[0-9]{1,7}$',
			 strtoupper(trim($_POST['nuevo_id_alarma']))))
	        || $_POST['estado'] != "2")
	      {
		$cquery .= ', nuevo_id_alarma = ' . sqlquote(strtoupper($_POST['nuevo_id_alarma']));
		$cquery .= ' WHERE id = ' . $id . ';';

		mysql_query($cquery);
	      }
	  }
      }

    $query = 'SELECT id, fecha, hora, fecha_devolucion, hora_devolucion, tipo_de_falla, persona_entrega, estado, persona_recibe, id_alarma, nuevo_id_alarma, usuario_crea FROM mantenimiento_de_alarmas WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }
?>
<div id="title"><span id="title_center">Ver mantenimiento de alarma</span></div>
<?php

if ($result)
  {
    $row = mysql_fetch_row($result);
    $estados = array("En reparaci&oacute;n", "Reparada", "Sustituida");

    $v = array('id' => $row[0],
	       'fecha' => $row[1] . ' ' . substr($row[2], 0, 5) ,
	       'fecha_devolucion' => $row[3] . ' ' . substr($row[4], 0, 5),
	       'tipo_de_falla' => $row[5],
	       'persona_entrega' => $row[6],
	       'estado' => $estados[$row[7]],
	       'persona_recibe' => $row[8],
	       'id_alarma' => $row[9],
	       'nuevo_id_alarma' => $row[10],
	       'usuario_crea' => $row[11]);

?>
    <form action="ver_mantenimiento_alarma.php" method="POST">
    <div id="form">
    <input type="hidden" style="display: none;" name="id" value=<? echo '"' . $row[0] . '"'; ?> />
    <label class="frm" for="fecha_ingreso">Fecha de Inicio:</label>
    <input class="frm" type="text" readonly="true" name="fecha_ingreso" id="fecha_ingreso"
           value=<? echo '"' . $v['fecha'] . '"'; ?> />
    <?php if ($row[7] != "0") { ?>
    <label class="frm" for="fecha_solicitud">Fecha de Entrega:</label>
    <input class="frm" type="text" readonly="true" name="fecha_devolucion" id="fecha_devolucion"
           value=<? echo '"' . $v['fecha_devolucion'] . '"'; ?> />
    <?php } else { ?>
    <br />
    <label class="frm" for="fecha_devolucion">Fecha de devoluci&oacute;n:</label>
    <script>DateInput('fecha_devolucion', true, 'YYYY-MM-DD')</script>
    <br />
    <label class="frm" for="hora_devolucion">Hora de devoluci&oacute;n:</label>
    <?php make_number_select("hora_devolucion_h", 0, 23, 0); ?>
    <?php make_number_select("hora_devolucion_m", 0, 59, 0); ?>
    <?php } ?>
    <br />
    <label class="frm" for="tipo_de_falla">Tipo de falla:</label>
    <?php make_input('tipo_de_falla', $v, true); ?>
    <br />
     <label class="frm" for="persona_entrega">Crucero:</label>
    <?php make_input('persona_entrega', $v, true); ?>
    <label class="frm" for="persona_recibe">Evento:</label>
    <?php make_input('persona_recibe', $v, $row[7] != "0"); ?>
    <br />
    <!--<label class="frm" for="id_alarma">ID de alarma:</label>-->
    <?php //make_input('id_alarma', $v, true); ?>
    <?php //if ($row[7] == 0 || $row[7] == "2") { ?>
    <!--<label class="frm" for="nuevo_id_alarma">Nuevo ID de alarma:</label>-->
    <?php //make_input('nuevo_id_alarma', $v, $row[7] != "0"); ?>
    <!--?php } ?>-->
    <br />
    <label class="frm" for="estado">Estado:</label>
    <?php
	 if ($row[7] != "0")
	   {
	     make_input('estado', $v, true);
           }
         else
           { ?>
      <select class="frm" id="estado" name="estado">
	<?php
	   $arr = array('estado' => $row[7]);
	   make_option("estado", "En reparaci&oacute;n", "0", $arr);
	   make_option("estado", "Reparada", "1", $arr); 
	   make_option("estado", "Sustituida", "2", $arr); 
	?>
      </select><br />
      <?php
	   }
	 if ($row[7] == "0") { ?>
      <input id="enviar" name="enviar" type="submit" value="Aplicar" />
      <?php } ?>
      <br />
    <?php
  }
else
  {
    echo '<div class="mensaje">Registro no encontrado.</div>' . "\n";
  }

echo '</div>';

include("footer.html");

if ($con)
  end_connection($con);
?>
