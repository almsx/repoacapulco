<?php
$allow = array(1 => true, 2 => true, 3 => true);


include('utils.inc');

$result = NULL;
$con = NULL;

$id = $_GET['id'];

if ($id)
  {
    $con = make_connection();

    $query = 'SELECT id, fecha, destinatario, destinatario_cargo, remitente, remitente_cargo, texto, destinatario_de_copia, folio FROM tarjetas_informativas WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }

if ($result)
  {
    $row = mysql_fetch_row($result);

    $v = array('id' => $row[0],
	       'fecha' => $row[1] ,
	       'destinatario' => uhtmlentities($row[2]),
	       'destinatario_cargo' => uhtmlentities($row[3]),
	       'remitente' => uhtmlentities($row[4]),
	       'remitente_cargo' => uhtmlentities($row[5]),
	       'remitente2' => uhtmlentities(strtoupper($row[4])),
	       'remitente_cargo2' => uhtmlentities(strtoupper($row[5])),
	       'texto' => nl2br(uhtmlentities($row[6])),
	       'destinatario_de_copia' => ereg_replace(",", "<br />", uhtmlentities($row[7])),
	       'folio' => $row[8]);
    $meses = array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre");
    sscanf($v['fecha'], "%d-%d-%d", $y, $m, $d);
    $mes = $meses[$m];
    $fecha = "$d de $mes del $y";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head><title>Tarjeta informativa</title></head>
  <body background="tarjeta.jpg">
    <table cellpadding="10" cellspacing="10">
      <tr>
	<td width="700">
	  <center><h3><b>TARJETA INFORMATIVA</b></h3></center>
          <p align="right"><?php echo $v['folio']; ?>
          <br />M&eacute;xico DF, a <?php echo $fecha; ?></p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>Para: <?php echo $v['destinatario']; ?><br />
	    <?php echo $v['destinatario_cargo']; ?></p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>De: <?php echo $v['remitente']; ?><br />
	    <?php echo $v['remitente_cargo']; ?></p>
	</td>
      <tr>
	<td height="300" valign="top">
	  <p align="justify"><?php echo $v['texto']; ?></p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>ATENTAMENTE</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p><?php echo $v['remitente_cargo2']; ?><br />
	    <?php echo $v['remitente2']; ?></p>
	</td>
      </tr>
      <?php if (0 < strlen($v['destinatario_de_copia'])) { ?>
      <tr>
	<td>
	  <p>c.c.p:<br /> <?php echo $v['destinatario_de_copia']; ?> </p>
	</td>
      </tr>
      <?php } ?>
    </table>
  </body>
</html>
<?php
  }
else
  {
    echo "Registro no encontrado.\n";
  }

if ($con)
  end_connection($con);
?>
