<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    if ((1 > strlen(trim($_POST['nombre']))))
      $errors[] = "Nombre de la escuela";
    if ((1 > strlen(trim($_POST['cuenta'])))
	|| !ereg('^[A-Z]{2}-[0-9]{1,7}$',
		 strtoupper(trim($_POST['cuenta']))))
      $errors[] = "ID de alarma";
    if (1 > strlen(trim($_POST['direccion'])))
      $errors[] = "Direcci&oacute;n";
    if (1 > strlen(trim($_POST['colonia'])))
      $errors[] = "Colonia";
    if (5 != strlen(trim($_POST['codigo_postal']))
	|| !is_numeric(trim($_POST['codigo_postal'])))
      $errors[] = "C&oacute;digo postal";
    $tel = ereg_replace("[ -]", "", trim($_POST['telefono']));
    if (8 > strlen($tel) || !is_numeric($tel))
      $errors[] = "Tel&eacute;fono";
    if (1 > strlen(trim($_POST['lugar_entrega'])))
      $errors[] = "Lugar de la entrega";
    if (1 > strlen(trim($_POST['persona_entrega'])))
      $errors[] = "Persona que entrega";
    if (1 > strlen(trim($_POST['persona_recibe'])))
      $errors[] = "Persona que recibe";
    
    $valid = count($errors) == 0;

    if ($valid)
      {
	$vars = 'nombre_de_la_escuela, direccion, colonia, codigo_postal, telefono, id_alarma, lugar_de_entrega, sector_id, persona_que_entrega, persona_que_recibe';
	
	$values = sqlquote(ucwords(strtolower($_POST['nombre']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['direccion']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['colonia']))) . ", " .
	  sqlquote($_POST['codigo_postal']) . ", " .
	  sqlquote($tel) . ", " .
	  sqlquote(strtoupper($_POST['cuenta'])) . ", " .
	  sqlquote(ucwords(strtolower($_POST['lugar_entrega']))) . ", " .
	  $_POST['sector'] . ", " .
	  sqlquote(ucwords(strtolower($_POST['persona_entrega']))) . ", " .
	  sqlquote(ucwords(strtolower($_POST['persona_recibe'])));

	
	$query = "INSERT INTO programa_escuela_segura (fecha, hora, $vars) " .
	  "values(CURDATE(), CURTIME(), $values);";
	mysql_query($query);
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=escuela_segura.php" />';
  }

include('header.html');

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="escuela_segura.php">Escuela Segura</a>: 
      </span>
      <span id="title_center">Nueva entrega en escuela</span>
    </div>
    <form action="form_escuela_segura.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>El contenido de los siguientes campos no es v&aacute;lido:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
      <input type="hidden" style="display: none;" name="commit" value="1" />
      <label class="frm" for="fecha">Fecha:</label>
      <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
	     value=<? echo '"' . date("d/m/Y H:i") . '"'; ?> />
      <br />
      <label class="frm" for="nombre">Nombre de la escuela:</label>
      <?php make_input('nombre'); ?>
      <label class="frm" for="direccion">Direcci&oacute;n:</label>
      <?php make_input('direccion'); ?>
      <br />
      <label class="frm" for="colonia">Colonia:</label>
      <?php make_input('colonia'); ?>
      <label class="frm" for="codigo_postal">C&oacute;digo postal:</label>
      <?php make_input('codigo_postal'); ?>
      <br />
      <label class="frm" for="telefono">Tel&eacute;fono:</label>
      <?php make_input('telefono'); ?>
      <label class="frm" for="cuenta">ID de alarma:</label>
      <?php make_input('cuenta'); ?>
      <br />
      <label class="frm" for="lugar_entrega">Lugar de la entrega:</label>
      <?php make_input('lugar_entrega'); ?>
      <label class="frm" for="sector">Sector:</label>
      <?php make_select('sector', 'sectores'); ?>
      <br />
      <label class="frm" for="persona_entrega">Persona que entrega:</label>
      <?php make_input('persona_entrega'); ?>
      <label class="frm" for="persona_recibe">Persona que recibe:</label>
      <?php make_input('persona_recibe'); ?>
      <br />
      <input id="enviar" name="enviar" type="submit" value="Enviar" />
      <br />
      </div>
    </form>

<?php
  }

include("footer.html");

end_connection($con);
?>
