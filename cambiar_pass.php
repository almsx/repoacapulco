<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    if (ereg('^$', $_POST['password']))
      $errors[] = "Contraseña inv&aacute;lida";
    if ($_POST['password'] != $_POST['password_again'])
      $errors[] = "Las contraseñas no coinciden";

    $valid = (count($errors) == 0);

    if ($valid)
      {
	$id = $_SESSION['login_id'];
	$newpass = sqlquote($_POST['password']);
	$query = "UPDATE usuarios SET pass = MD5($newpass) WHERE id = $id;";

	mysql_query($query);
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=admin.php" />';
  }

include("header.html");

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
      </span>
      <span id="title_center">Cambiar contraseña</span>
    </div>
    <form action="cambiar_pass.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>No se pudo completar la acci&oacute;n debido a los siguientes errores:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
        <input type="hidden" style="display: none;" name="commit" value="1" />
	<label class="frm" for="password">Contraseña:</label>
	<input class="frm" type="password" id="password" name="password" value="" />
        <label class="frm" for="password">Contraseña (repetir):</label>
	<input class="frm" type="password" id="password_again" name="password_again" value="" />
	<br />
	<br />
	<br />
	<input id="cambiar" name="cambiar" type="submit" value="Cambiar" />
	<br />
      </div>
    </form>

<?php
  }
include("footer.html");
?>
