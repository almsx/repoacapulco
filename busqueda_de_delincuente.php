<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

if (!$_GET['pdf'])
  include('session.inc');
include("utils.inc");

$con = make_connection();

$results = NULL;

if ($_GET['buscar'])
  {
    $filters = array();
    $filter_string = "";
    $query = NULL;

    $hora_inicio = $_GET['hora_inicio_h'] . ':' . $_GET['hora_inicio_m'] . ':00';
    $hora_fin = $_GET['hora_fin_h'] . ':' . $_GET['hora_fin_m'] . ':59';

    if ($_GET['hora_inicio'])
      $filters[] = 'hora >= ' . sqlquote($hora_inicio);
    if ($_GET['hora_fin'])
      $filters[] = 'hora <= ' . sqlquote($hora_fin);
    if ($_GET['fecha_inicio'] && $_GET['fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha >= ' . sqlquote($_GET['fecha_inicio']);
    if ($_GET['fecha_fin'] && $_GET['fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha <= ' . sqlquote($_GET['fecha_fin']);
    if (0 < strlen(trim($_GET['cuenta'])))
      $filters[] = 'id_alarma LIKE ' . sqlquote('%' . strtoupper($_GET['cuenta']) . '%');
    if (0 < strlen(trim($_GET['nombre'])))
      $filters[] = 'nombre LIKE ' . sqlquote('%' . ucwords(strtolower($_GET['nombre'])) . '%');
    if (0 < strlen(trim($_GET['apellido'])))
      $filters[] = 'apellido LIKE ' . sqlquote('%' . ucwords(strtolower($_GET['apellido'])) . '%');
    if (0 < strlen(trim($_GET['numero'])))
      $filters[] = 'numero_de_beeper = ' . $_GET['numero'];
    if (0 < strlen(trim($_GET['telefono'])))
      $filters[] = 'telefono LIKE ' . sqlquote('%' . $_GET['telefono'] . '%');

    if (count($filters) > 0)
      $filterstr .= "WHERE " . implode(" AND ", $filters);

    $query = "SELECT id, fecha, hora, nombre, apellido, id_alarma, numero_de_beeper FROM entrega_de_beepers $filterstr;";

    $results = mysql_query($query);
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");

?>
    <?php if (!$_GET['pdf']) { ?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="criminalidad.php">Criminalidad</a>:
      </span>
      <span id="title_center">B&uacute;squeda</span>
    </div>
    <form action="busqueda_de_delincuente.php" method="GET">
      <div id="form">
      <label class="frm" for="fecha_inicio">Fecha inicial:</label>
      <script>DateInput('fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="fecha_fin">Fecha final:</label>
      <script>DateInput('fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="hora_inicio_h">Horario (HH:MM):</label>
      <label class="frm" style="width: auto; margin-left: 20px; " for="hora_inicio_h">desde:</label>
      <input class="cb" type="checkbox" id="hora_inicio" name="hora_inicio"
             value="1"
	     <?php if ($_GET['hora_inicio'] == "1")
                     echo 'checked="true"'; ?> />
    <?php make_number_select("hora_inicio_h", 0, 23, 0); ?>
    <?php make_number_select("hora_inicio_m", 0, 59, 0); ?>
      <label class="frm" style="width: auto; margin-left: 20px; " for="hora_fin_h">hasta:</label>
      <input class="cb" type="checkbox" id="hora_fin" name="hora_fin"
             value="1"
	     <?php if ($_GET['hora_fin'] == "1")
                     echo 'checked="true"'; ?> />
      <?php make_number_select("hora_fin_h", 0, 23, 23); ?>
      <?php make_number_select("hora_fin_m", 0, 59, 59); ?>
      <br />
      <label class="frm" for="nombre">Nombre:</label>
      <?php make_input('nombre', $_GET); ?>
      <label class="frm" for="apellido">Apellido:</label>
      <?php make_input('apellido', $_GET); ?>
      <br />
      <label class="frm" for="telefono">Tel&eacute;fono:</label>
      <?php make_input('telefono', $_GET); ?>
      <br />
      <label class="frm" for="cuenta">ID del Detenido:</label>
      <?php make_input('cuenta', $_GET); ?>
      <label class="frm" for="numero">N&uacute;mero :</label>
      <?php make_input('numero', $_GET); ?>
      <br />
      <input id="buscar" name="buscar" type="submit" value="Buscar" />
      <br />
      </div>
    </form>
    <?php
      if ($_GET['buscar'])
	{
	  $url = "buscar_beeper.php?" . urlencode(http_build_query($_GET));
	  echo "<a target=\"_blank\" href=\"make_pdf.php?url=$url\">Exportar a PDF</a>";
	}
      }
    ?>
    <?php if ($_GET['pdf']) { echo '<center><h3>Entregas de beepers</h3></center>' . "\n"; } ?>
    <hr />
<?php

if ($results)
  {
    $headers = array("ID", "Fecha y Hora", "Nombre", "ID de alarma", "N&uacute;mero de beeper");

    echo '<div id="search_results">' . "\n";
    echo '<table id="search_results">' . "\n";
    echo '<tr class="header">';
    
    foreach ($headers as $hd)
      echo '<th class="header">' . $hd . '</th>';

    $medios_de_recepcion = get_poplist_table_array("medios_de_recepcion");
    $tipos_de_suceso = get_poplist_table_array("tipos_de_suceso");

    $num = 0;

    while ($row = mysql_fetch_row($results))
      {
	echo '<tr onClick="window.open(\'ver_entrega_de_beeper.php?id=' .
	  $row[0] . '\', \'Suceso\', \'width=730,height=500,status=0,toolbar=0\');" onmouseover="this.style.backgroundColor = \'#ffffff\';" onmouseout="this.style.backgroundColor = \'#f5f1e8\';" class="result">';

	echo '<td class="result_cell">' . $row[0] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[1] . " " . $row[2] . '</td>' . "\n";
	echo '<td class="result_cell">' . uhtmlentities($row[3] . ' ' . $row[4]) . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[5] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[6] . '</td>' . "\n";
	echo '</tr>' . "\n";
	$num += 1;
      }
    echo '</table>' . "\n";
    echo "<br /><p>Total de registros: $num</p>";
    echo '</div>' . "\n";
  }

include("footer.html");

end_connection($con);
?>
