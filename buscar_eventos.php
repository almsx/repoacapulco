<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);


if (!$_GET['pdf'])
  include('session.inc');
include("utils.inc");

$con = make_connection();

$results = NULL;

if ($_GET['buscar'])
  {
    $filters = array();
    $filter_string = "";
    $query = NULL;

    if ($_GET['ingreso_fecha_inicio'] && $_GET['ingreso_fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha >= ' . sqlquote($_GET['ingreso_fecha_inicio']);
    if ($_GET['ingreso_fecha_fin'] && $_GET['ingreso_fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha <= ' . sqlquote($_GET['ingreso_fecha_fin']);
    if ($_GET['evento_fecha_inicio'] && $_GET['evento_fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha_evento >= ' . sqlquote($_GET['evento_fecha_inicio']);
    if ($_GET['evento_fecha_fin'] && $_GET['evento_fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha_evento <= ' . sqlquote($_GET['evento_fecha_fin']);
    if (0 < strlen(trim($_GET['tipo_evento'])))
      $filters[] = 'tipo LIKE ' . sqlquote('%' . strtoupper($_GET['tipo_evento']) . '%');
    if (0 < strlen(trim($_GET['usuario_captura'])))
      $filters[] = 'usuario_crea LIKE ' . sqlquote('%' . $_GET['usuario_captura'] . '%');
    if ($_GET['escuela_segura'] && $_GET['escuela_segura'] != "-1")
      $filters[] = 'escuela_segura = ' . $_GET['escuela_segura'];

    if (count($filters) > 0)
      $filterstr .= "WHERE " . implode(" AND ", $filters);

    $query = "SELECT id, fecha, hora, fecha_evento, hora_evento, tipo, escuela_segura FROM eventos $filterstr;";

    $results = mysql_query($query);
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");

?>
    <?php if (!$_GET['pdf']) { ?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="eventos.php">Eventos</a>:
      </span>
      <span id="title_center">B&uacute;squeda</span>
    </div>
    <form action="buscar_eventos.php" method="GET">
      <div id="form">
      <label class="frm" for="ingreso_fecha_inicio">Fecha de ingreso inicial:</label>
      <script>DateInput('ingreso_fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="ingreso_fecha_fin">Fecha de ingreso final:</label>
      <script>DateInput('ingreso_fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="evento_fecha_inicio">Fecha del evento inicial:</label>
      <script>DateInput('evento_fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="evento_fecha_fin">Fecha del evento final:</label>
      <script>DateInput('evento_fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="tipo_evento">Tipo de evento:</label>
      <?php make_input('tipo_evento', $_GET); ?>
      <label class="frm" for="escuela_segura">Es escuela segura:</label>
      <select class="frm" id="escuela_segura" name="escuela_segura">
	<?php
	   make_option("escuela_segura", "-", "-1", $_GET);
	   make_option("escuela_segura", "No", "0", $_GET);
	   make_option("escuela_segura", "Si", "1", $_GET);
	?>
      </select><br />
      <label class="frm" for="usuario_captura">Quien captur&oacute;:</label>
      <?php make_input('usuario_captura', $_GET); ?>
      <br />
      <input id="buscar" name="buscar" type="submit" value="Buscar" />
      <br />
      </div>
    </form>
    <?php
      if ($_GET['buscar'])
	{
	  $url = "buscar_eventos.php?" . urlencode(http_build_query($_GET));
	  echo "<a target=\"_blank\" href=\"make_pdf.php?url=$url\">Exportar a PDF</a>";
	}
      }
    ?>
    <?php if ($_GET['pdf']) { echo '<center><h3>Eventos</h3></center>' . "\n"; } ?>
    <hr />
<?php

if ($results)
  {
    $headers = array("ID", "Fecha de ingreso", "Fecha del evento", "Tipo de evento", "Es escuela segura");

    echo '<div id="search_results">' . "\n";
    echo '<table id="search_results">' . "\n";
    echo '<tr class="header">';
    
    foreach ($headers as $hd)
      echo '<th class="header">' . $hd . '</th>';

    $num = 0;

    while ($row = mysql_fetch_row($results))
      {
	echo '<tr onClick="window.open(\'ver_evento.php?id=' .
	  $row[0] . '\', \'Suceso\', \'width=730,height=500,status=0,toolbar=0\');" onmouseover="this.style.backgroundColor = \'#ffffff\';" onmouseout="this.style.backgroundColor = \'#f5f1e8\';" class="result">';

	echo '<td class="result_cell">' . $row[0] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[1] . ' ' . substr($row[2], 0, 5) . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[3] . ' ' . substr($row[4], 0, 5) . '</td>' . "\n";
 	echo '<td class="result_cell">' . uhtmlentities($row[5]) . '</td>' . "\n";

	echo '<td class="result_cell">' . ($row[6] == "0" ? "No" : "Si") . '</td>' . "\n";
	echo '</tr>' . "\n";
	$num += 1;
      }
    echo '</table>' . "\n";
    echo "<br /><p>Total de registros: $num</p>";
    echo '</div>' . "\n";
  }

include("footer.html");

end_connection($con);
?>
