<?php
$allow = array(1 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    $exists = mysql_fetch_row(mysql_query('SELECT id FROM sectores WHERE descripcion = "' . $_POST['nombre'] . '";'));

    if ($exists)
      $errors[] = "Ya existe un sector con ese nombre";
    if (1 > strlen($_POST['nombre'])
	|| trim($_POST['nombre']) != $_POST['nombre'] )
      $errors[] = "Nombre de sector inv&aacute;lido";

    $valid = (count($errors) == 0);

    if ($valid)
      {
	$vars = 'descripcion';
	$values = sqlquote($_POST['nombre']);
	$query = "INSERT INTO sectores ($vars) values($values);";

	mysql_query($query);
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=admin_sectores.php" />';
  }

include("header.html");

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="admin.php">Administrador</a>: 
        <a class="title_link" href="admin_sectores.php">Sectores</a>: 
      </span>
      <span id="title_center">Nuevo</span>
    </div>
    <form action="nuevo_sector.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>No se pudo completar la acci&oacute;n debido a los siguiente errores:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
        <input type="hidden" style="display: none;" name="commit" value="1" />
	<label class="frm" for="nombre">Nombre del sector:</label>
	<?php make_input('nombre'); ?>
	<br />
	<br />
	<br />
	<input id="crear" name="crear" type="submit" value="Crear" />
	<br />
      </div>
    </form>

<?php
  }
include("footer.html");
?>
