DROP TABLE tipos_de_suceso;
CREATE TABLE tipos_de_suceso (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL UNIQUE,
  borrado BOOL NOT NULL default 0,
  PRIMARY KEY (id));
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(1, "Escándalo/Drogas/Ingerir");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(2, "Accidente Automovilístico");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(3, "Accidente Enfermedad");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(4, "Lesionado");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(5, "Problemas Urbanos");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(6, "Problemas Familiares");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(7, "Robo");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(8, "Tránsito");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(9, "Muerte");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(10, "Riña");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(11, "Incendio");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(12, "Fuga Agua/Gas");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(13, "Manifestación");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(14, "Sospechoso");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(15, "Avispas");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(16, "Alarma Sin Respuesta");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(17, "Seguimiento Escuela Segura");
INSERT INTO tipos_de_suceso (id, descripcion) VALUES(18, "Otra");

DROP TABLE canalizaciones;
CREATE TABLE canalizaciones (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL UNIQUE,
  borrado BOOL NOT NULL default 0,
  PRIMARY KEY (id));
INSERT INTO canalizaciones (id, descripcion) VALUES(1, "Policía SSP");
INSERT INTO canalizaciones (id, descripcion) VALUES(2, "Ambulancia/Protección Civil");
INSERT INTO canalizaciones (id, descripcion) VALUES(3, "Servicios Urbanos");
INSERT INTO canalizaciones (id, descripcion) VALUES(4, "Bomberos");

DROP TABLE tipos_de_registro;
CREATE TABLE tipos_de_registro (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL UNIQUE,
  PRIMARY KEY (id));

DROP TABLE turnos;
CREATE TABLE turnos (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL UNIQUE,
  PRIMARY KEY (id));

DROP TABLE medios_de_recepcion;
CREATE TABLE medios_de_recepcion (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL,
  borrado BOOL NOT NULL default 0,
  PRIMARY KEY (id));
INSERT INTO medios_de_recepcion (id, descripcion) VALUES(1, "Alarma");
INSERT INTO medios_de_recepcion (id, descripcion) VALUES(2, "Teléfono");
INSERT INTO medios_de_recepcion (id, descripcion) VALUES(3, "Radio");
INSERT INTO medios_de_recepcion (id, descripcion) VALUES(4, "Monitor");

DROP TABLE sectores;
CREATE TABLE sectores (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL,
  borrado BOOL NOT NULL default 0,
  PRIMARY KEY (id));
INSERT INTO sectores (id, descripcion) VALUES(1, "Hormiga");
INSERT INTO sectores (id, descripcion) VALUES(2, "Claveria");
INSERT INTO sectores (id, descripcion) VALUES(3, "Cuitlahuac");
INSERT INTO sectores (id, descripcion) VALUES(4, "La Raza");

DROP TABLE colonias;
CREATE TABLE colonias (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL,
  sector_id INTEGER NOT NULL REFERENCES sectores(id),
  borrado BOOL NOT NULL default 0,
  PRIMARY KEY (id));

DROP TABLE camaras;
CREATE TABLE camaras (
  id INTEGER NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(256) NOT NULL,
  borrado BOOL NOT NULL default 0,
  PRIMARY KEY (id));
INSERT INTO camaras (id, descripcion) VALUES(1, "C-1 Aquiles Serdan y Av. Culturas");
INSERT INTO camaras (id, descripcion) VALUES(2, "C-2 Entrada Metro Rosario Anden F");
INSERT INTO camaras (id, descripcion) VALUES(3, "C-3 Interior Parque Tezozomoc");
INSERT INTO camaras (id, descripcion) VALUES(4, "C-4 Eje 5 y Av. Granjas");
INSERT INTO camaras (id, descripcion) VALUES(5, "C-5 Av. Tezozomoc y Aquiles Serdan");
INSERT INTO camaras (id, descripcion) VALUES(6, "C-6 Delegacion Azcapotzalco");
INSERT INTO camaras (id, descripcion) VALUES(7, "C-7 Las Culturas y Cultura Griega");
INSERT INTO camaras (id, descripcion) VALUES(8, "C-8 Av. Azcapotzalco y Aztecas");
INSERT INTO camaras (id, descripcion) VALUES(9, "C-9 Av. Azcapotzalco y Esperanza");
INSERT INTO camaras (id, descripcion) VALUES(10, "C-10 Camarones y 22 de Febrero");
INSERT INTO camaras (id, descripcion) VALUES(11, "C-11 5 de Mayo y Tezozomoc");
INSERT INTO camaras (id, descripcion) VALUES(12, "C-12 Camarones y Heliopolis");
INSERT INTO camaras (id, descripcion) VALUES(13, "C-13 Parque de la China");
INSERT INTO camaras (id, descripcion) VALUES(14, "C-14 Eje 3 y San Isidro");
INSERT INTO camaras (id, descripcion) VALUES(15, "C-15 Macario Glaxolia y Las Armas");
INSERT INTO camaras (id, descripcion) VALUES(16, "C-16 Jardin y Circuito Interior");
INSERT INTO camaras (id, descripcion) VALUES(17, "C-17 Camarones y Alheli");
INSERT INTO camaras (id, descripcion) VALUES(18, "C-18 Cuitlahuac y Prol. Nueces");
INSERT INTO camaras (id, descripcion) VALUES(19, "C-19 Cainito y Tlatilco");
INSERT INTO camaras (id, descripcion) VALUES(20, "C-20 Poniente 140 y Norte 45");
INSERT INTO camaras (id, descripcion) VALUES(21, "C-21 Vallejo y Antonio Valeriano");
INSERT INTO camaras (id, descripcion) VALUES(22, "C-22 Cuitlahuac y Jardin");
INSERT INTO camaras (id, descripcion) VALUES(23, "C-23 Vallejo y Poniente 146");
INSERT INTO camaras (id, descripcion) VALUES(24, "C-24 Cuitlahuac y Vallejo");
INSERT INTO camaras (id, descripcion) VALUES(25, "C-25 Av. Ceylan y Poniente 144");
INSERT INTO camaras (id, descripcion) VALUES(26, "C-26 Norte 59 y Poniente 152");
INSERT INTO camaras (id, descripcion) VALUES(27, "C-27 Jardín P.C.");

DROP TABLE sucesos;
CREATE TABLE sucesos (
  id INTEGER NOT NULL AUTO_INCREMENT,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  medio_de_recepcion_id INTEGER NOT NULL REFERENCES medios_de_recepcion(id),
  tipo_de_suceso_id INTEGER NOT NULL REFERENCES tipos_de_suceso(id),
  canalizacion_id INTEGER NOT NULL REFERENCES canalizaciones(id),
  proteccion_civil VARCHAR(256),
  seguridad_publica VARCHAR(256),
  turno INTEGER NOT NULL,
  tiempo_care TIME NOT NULL,
  tiempo_base_plata TIME NOT NULL,
  notas_abre TEXT,
  notas_cierra TEXT,
  suceso_relevante BOOL NOT NULL,
  tipo_de_registro_id INTEGER NOT NULL REFERENCES tipos_de_registro(id),
  abierto BOOL NOT NULL,
  usuario_crea VARCHAR(128) NOT NULL,
  perfil_usuario_crea VARCHAR(128) NOT NULL,
  usuario_cierra VARCHAR(128) NOT NULL,
  perfil_usuario_cierra VARCHAR(128) NOT NULL,
  PRIMARY KEY (id));

DROP TABLE eventos_alarmas;
CREATE TABLE eventos_alarmas (
  id INTEGER NOT NULL REFERENCES sucesos(id),
  cuenta CHAR(10) NOT NULL,
  cap_code INTEGER NOT NULL,
  PRIMARY KEY (id));

DROP TABLE eventos_radio_telefono;
CREATE TABLE eventos_radio_telefono (
  id INTEGER NOT NULL REFERENCES sucesos(id),
  direccion VARCHAR(256) NOT NULL,
  colonia VARCHAR(256) NOT NULL,
  sector_id INTEGER NOT NULL REFERENCES sectores(id),
  telefono VARCHAR(32),
  calles VARCHAR(256),
  PRIMARY KEY (id));
/*
ALTER TABLE eventos_radio_telefono ADD COLUMN calles VARCHAR(256) DEFAULT "";
*/

DROP TABLE eventos_monitores;
CREATE TABLE eventos_monitores (
  id INTEGER NOT NULL REFERENCES sucesos(id),
  camara_id INTEGER NOT NULL REFERENCES camaras(id),
  PRIMARY KEY (id));

DROP TABLE  entrega_de_beepers;
CREATE TABLE  entrega_de_beepers (
  id INTEGER NOT NULL AUTO_INCREMENT,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  nombre VARCHAR(256) NOT NULL,
  apellido VARCHAR(256) NOT NULL,
  direccion VARCHAR(256) NOT NULL,
  telefono VARCHAR(32) NOT NULL,
  id_alarma CHAR(10) NOT NULL,
  numero_de_beeper INTEGER NOT NULL UNIQUE,
  lugar_de_entrega VARCHAR(256) NOT NULL,
  nombre_entrega VARCHAR(256) NOT NULL,
  PRIMARY KEY (id));


DROP TABLE  programa_escuela_segura;
CREATE TABLE  programa_escuela_segura (
  id INTEGER NOT NULL AUTO_INCREMENT,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  nombre_de_la_escuela VARCHAR(256) NOT NULL,
  direccion VARCHAR(256) NOT NULL,
  colonia VARCHAR(256) NOT NULL,
  codigo_postal VARCHAR(32) NOT NULL,
  telefono VARCHAR(32) NOT NULL,
  id_alarma CHAR(10) NOT NULL,
  lugar_de_entrega VARCHAR(256) NOT NULL,
  sector_id INTEGER NOT NULL REFERENCES sectores(id),
  persona_que_entrega VARCHAR(256) NOT NULL,
  persona_que_recibe VARCHAR(256) NOT NULL,
  PRIMARY KEY (id));

DROP TABLE solicitud_de_alarmas;
CREATE TABLE solicitud_de_alarmas (
  id INTEGER NOT NULL AUTO_INCREMENT,
  folio VARCHAR(12) NOT NULL,
  nombre_solicitante VARCHAR(256) NOT NULL,
  nombre_recibe VARCHAR(256) NOT NULL,
  nombre_entrega VARCHAR(256) NOT NULL,
  domicilio VARCHAR(256) NOT NULL,
  colonia VARCHAR(256) NOT NULL,
  telefono VARCHAR(32) NOT NULL,
  fecha_de_solicitud DATE NOT NULL,
  fecha_de_ingreso DATE NOT NULL,
  id_alarma CHAR(10) NOT NULL,
  fecha_de_entrega DATE NOT NULL,
  entregada BOOL NOT NULL,
  PRIMARY KEY (id));

DROP TABLE alarmas;
CREATE TABLE alarmas (
  id INTEGER NOT NULL AUTO_INCREMENT,
  id_alarma CHAR(10) NOT NULL UNIQUE,
  persona VARCHAR(256) NOT NULL,
  colonia VARCHAR(256) NOT NULL,
  domicilio VARCHAR(256) NOT NULL,
  telefono VARCHAR(32) NOT NULL,
  cap_code INTEGER NOT NULL,
  activa BOOL NOT NULL DEFAULT TRUE,
  PRIMARY KEY (id));

DROP TABLE tarjetas_informativas;
CREATE TABLE tarjetas_informativas (
  id INTEGER NOT NULL AUTO_INCREMENT,
  fecha DATE NOT NULL,
  folio VARCHAR(128) NOT NULL,
  destinatario VARCHAR(128) NOT NULL,
  destinatario_cargo VARCHAR(128) NOT NULL,
  remitente VARCHAR(128) NOT NULL,
  remitente_cargo VARCHAR(128) NOT NULL,
  texto TEXT NOT NULL,
  destinatario_de_copia VARCHAR(128) NOT NULL ,
  PRIMARY KEY (id));

DROP TABLE prueba_de_alarmas;
CREATE TABLE prueba_de_alarmas (
  id INTEGER NOT NULL AUTO_INCREMENT,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  id_alarma CHAR(10) NOT NULL,
  tipo_de_domicilio INTEGER NOT NULL,
  persona_prueba VARCHAR(128) NOT NULL,
  resultado INTEGER NOT NULL,
  notas TEXT,
  PRIMARY KEY (id));

DROP TABLE mantenimiento_de_alarmas;
CREATE TABLE mantenimiento_de_alarmas (
  id INTEGER NOT NULL AUTO_INCREMENT,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  id_alarma CHAR(10) NOT NULL,
  tipo_de_falla VARCHAR(256) NOT NULL,
  persona_entrega VARCHAR(128) NOT NULL,
  estado INTEGER NOT NULL,
  fecha_devolucion DATE NOT NULL,
  hora_devolucion TIME NOT NULL,
  persona_recibe VARCHAR(128) NOT NULL,
  nuevo_id_alarma CHAR(10),
  usuario_crea VARCHAR(128) NOT NULL,
  PRIMARY KEY (id));

DROP TABLE eventos;
CREATE TABLE eventos (
  id INTEGER NOT NULL AUTO_INCREMENT,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  fecha_evento DATE NOT NULL,
  hora_evento TIME NOT NULL,
  tipo VARCHAR(256) NOT NULL,
  lugar VARCHAR(256) NOT NULL,
  visitante VARCHAR(256) NOT NULL,
  escuela_segura BOOL NOT NULL,
  usuario_crea VARCHAR(128) NOT NULL,
  notas TEXT,
  PRIMARY KEY (id));

DROP TABLE usuarios;
CREATE TABLE usuarios (
  id INTEGER NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(32) NOT NULL UNIQUE,
  pass CHAR(32) NOT NULL,
  perfil INTEGER NOT NULL,
  nombre_persona VARCHAR(128) NOT NULL,
  apellidos VARCHAR(128) NOT NULL,
  telefono VARCHAR(32) NOT NULL,
  direccion VARCHAR(256) NOT NULL,
  dependencia VARCHAR(256) NOT NULL,
  PRIMARY KEY (id));
INSERT INTO usuarios (nombre, pass, perfil, nombre_persona, apellidos, telefono, direccion, dependencia) values("admincare", md5("ok"), 1, "", "", "", "", "");

/*
ALTER TABLE usuarios ADD COLUMN nombre_persona VARCHAR(128) NOT NULL DEFAULT "*";
ALTER TABLE usuarios ADD COLUMN apellidos VARCHAR(128) NOT NULL DEFAULT "*";
ALTER TABLE usuarios ADD COLUMN telefono VARCHAR(32) NOT NULL DEFAULT "*";
ALTER TABLE usuarios ADD COLUMN direccion VARCHAR(256) NOT NULL DEFAULT "*";
ALTER TABLE usuarios ADD COLUMN dependencia VARCHAR(256) NOT NULL DEFAULT "*";
*/
