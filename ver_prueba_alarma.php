<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

include('session.inc');
include('utils.inc');

$noexit = true;
$print = true;

include('header.html');

$result = NULL;
$con = NULL;

$id = $_GET['id'];

if ($id)
  {
    $con = make_connection();

    $query = 'SELECT id, fecha, hora, id_alarma, tipo_de_domicilio, persona_prueba, resultado, notas FROM prueba_de_alarmas WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }
?>
<div id="title"><span id="title_center">Ver prueba de alarma</span></div>
<?php

if ($result)
  {
    $row = mysql_fetch_row($result);
    $tipos_domicilio = array("Casa", "Comercio", "Escuela Segura");
    $resultados = array("Fallido", "Exitoso");

    $v = array('id' => $row[0],
	       'fecha' => $row[1] . ' ' . $row[2],
	       'cuenta' => $row[3],
	       'tipo_domicilio' => $tipos_domicilio[$row[4]],
	       'persona_prueba' => uhtmlentities($row[5]),
	       'resultado' => $resultados[$row[6]],
	       'notas' => uhtmlentities($row[7]));

?>
    <form action="ver_prueba_alarma.php" method="POST">
    <div id="form">
    <input type="hidden" style="display: none;" name="id" value=<? echo '"' . $row[0] . '"'; ?> />
    <label class="frm" for="fecha">Fecha:</label>
    <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
           value=<? echo '"' . $row[1] . ' ' . $row[2] . '"'; ?> />
    <br />
    <label class="frm" for="cuenta">ID de alarma:</label>
    <?php make_input('cuenta', $v, true); ?>
    <label class="frm" for="tipo_domicilio">Tipo de domicilio:</label>
    <?php make_input('tipo_domicilio', $v, true); ?>
    <br />
    <label class="frm" for="persona_prueba">Con quien se hizo la prueba:</label>
    <?php make_input('persona_prueba', $v, true); ?>
    <label class="frm" for="resultado">Resultado:</label>
    <?php make_input('resultado', $v, true); ?>
    <br />
    <label class="frm" for="notas">Notas:</label>
    <textarea class="frm" id="notas" readonly="true" name="notas"
      ><?php echo $v['notas']; ?></textarea><br />
    <?php
  }
else
  {
    echo '<div class="mensaje">Registro no encontrado.</div>' . "\n";
  }

echo '</div>';

include("footer.html");

if ($con)
  end_connection($con);
?>
