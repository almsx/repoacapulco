<?php
$allow = array(1 => true, 2 => true, 3 => true);

include('utils.inc');

$result = NULL;
$con = make_connection();

$fecha_inicio = $_GET['fecha_inicio'];
$fecha_fin = $_GET['fecha_fin'];

$sum_medios = "";
$sum_sucesos = "";
$sum_canalizaciones = "";

$nmedios = get_poplist_table_array("medios_de_recepcion");
$ncanalizaciones = get_poplist_table_array("canalizaciones");
$nsucesos = get_poplist_table_array("tipos_de_suceso");

$counts = array();

foreach (array_keys($nmedios) as $id)
  $counts[] = "SUM(medio_de_recepcion_id = $id)";

$counts_sel = implode(", ", $counts);

$cond = "WHERE fecha >= '$fecha_inicio' AND fecha <= '$fecha_fin'";
$ctotal_query = "SELECT COUNT(*) FROM sucesos $cond;";
$cmedios_query = "SELECT medio_de_recepcion_id, COUNT(*) FROM sucesos $cond GROUP BY medio_de_recepcion_id ORDER BY medio_de_recepcion_id ASC;";
$csucesos_query = "SELECT tipo_de_suceso_id, COUNT(*) FROM sucesos $cond GROUP BY tipo_de_suceso_id ORDER BY tipo_de_suceso_id ASC;";
$ccanalizaciones_query = "SELECT canalizacion_id, COUNT(*) FROM sucesos $cond GROUP BY canalizacion_id ORDER BY canalizacion_id ASC;";
$cmedios_por_suceso_query = "SELECT tipo_de_suceso_id, medio_de_recepcion_id, COUNT(*), $counts_sel FROM sucesos $cond GROUP BY tipo_de_suceso_id ORDER BY tipo_de_suceso_id ASC;";

$ctotal_results = result_to_array(mysql_query($ctotal_query));
$ctotal = $ctotal_results[0][0];
$cmedios_result = result_to_array(mysql_query($cmedios_query));
$csucesos_result = result_to_array(mysql_query($csucesos_query));
$ccanalizaciones_result = result_to_array(mysql_query($ccanalizaciones_query));
$cmedios_por_suceso_result = result_to_array(mysql_query($cmedios_por_suceso_query));

function make_ref_img($i)
{
  $ref = $i * 3 + 2;

  return "<img src=\"./gp/graphref.php?ref=$ref&typ=2&dim=6\" />";  
}

function print_medios_de_recepcion_graph($r)
{
  global $nmedios;
  $medios = array();
  $vals = array();
  $ids = array_keys($nmedios);

  foreach ($r as $row)
    $medios[$row[0]] = $row[1];  

  foreach ($ids as $id)
    $vals[$id] = ($medios[$id] ? $medios[$id] : 0);

  $imgurl = './gp/graphbarras.php?dat=' . implode(",", array_values($vals));
  echo "<p>Emergencias por tipo de recepci&oacute;n</p>";
  echo '<img src="' . $imgurl . '" />' . "\n";
}

function print_medios_de_recepcion_table($r, $total)
{
  global $nmedios;
  $medios = array();
  $vals = array();
  $ids = array_keys($nmedios);

  foreach ($r as $row)
    $medios[$row[0]] = $row[1];  

  foreach ($ids as $id)
    $vals[$id] = ($medios[$id] ? $medios[$id] : 0);

  echo '<table border="1"><tr><th></th><th>Recepci&oacute;n</th><th>N&uacute;mero de emergencias</th></tr>';

  $x = 1;
  foreach (array_keys($nmedios) as $i)
    {
      $name = uhtmlentities($nmedios[$i]);
      $val = $vals[$i];
      $img = make_ref_img($x % 10);
      echo "<tr><td>$img</td><td>$name</td><td align=\"right\">$val</td></tr>";
      $x += 1;
    }

  echo "<tr><td></td><td>Total</td><td align=\"right\">$total</td></tr>";
  echo '</table>';
}

function print_canalizaciones_graph($r)
{
  global $ncanalizaciones;
  $canalizaciones = array();
  $vals = array();
  $ids = array_keys($ncanalizaciones);

  foreach ($r as $row)
    $canalizaciones[$row[0]] = $row[1];  

  foreach ($ids as $id)
    $vals[$id] = ($canalizaciones[$id] ? $canalizaciones[$id] : 0);

  $imgurl = './gp/graphbarras.php?dat=' . implode(",", array_values($vals));
  echo '<p>Emergencias por tipo de apoyo</p>';
  echo '<img src="' . $imgurl . '" />' . "\n";
}

function print_canalizaciones_table($r, $total)
{
  global $ncanalizaciones;
  $canalizaciones = array();
  $vals = array();
  $ids = array_keys($ncanalizaciones);

  foreach ($r as $row)
    $canalizaciones[$row[0]] = $row[1];  

  foreach ($ids as $id)
    $vals[$id] = ($canalizaciones[$id] ? $canalizaciones[$id] : 0);

  echo '<table border="1"><tr><th></th><th>Tipo de apoyo</th><th>N&uacute;mero de emergencias</th></tr>';

  $x = 1;
  foreach (array_keys($ncanalizaciones) as $i)
    {
      $name = uhtmlentities($ncanalizaciones[$i]);
      $val = $vals[$i];
      $img = make_ref_img($x % 10);
      echo "<tr><td>$img</td><td>$name</td><td align=\"right\">$val</td></tr>";
      $x += 1;
    }

  echo "<tr><td></td><td>Total</td><td align=\"right\">$total</td></tr>";
  echo '</table>';
}

function print_sucesos_graph($r)
{
  global $nsucesos;
  $sucesos = array();
  $vals = array();
  $ids = array_keys($nsucesos);

  foreach ($r as $row)
    $sucesos[$row[0]] = $row[1];  

  foreach ($ids as $id)
    $vals[$id] = ($sucesos[$id] ? $sucesos[$id] : 0);

  $imgurl = './gp/graphbarras.php?dat=' . implode(",", array_values($vals));
  echo '<img src="' . $imgurl . '" />' . "\n";
}

function print_sucesos_table($r)
{
  global $nmedios;
  global $nsucesos;
  $vals = array();
  $ids = array_keys($nmedios);

  echo '<table border="1"><tr><th></th><th>Tipo de suceso</th>';

  foreach(array_values($nmedios) as $name)
    {
      $name = uhtmlentities($name);
      echo "<th>$name</th>";
    }

  foreach (array_keys($r) as $x)
    $vals[$r[$x][0]] = $r[$x];

  echo '<th>N&uacute;mero de sucesos</th></tr>';

  $x = 1;
  foreach (array_keys($nsucesos) as $i)
    {
      $name = uhtmlentities($nsucesos[$i]);
      $img = make_ref_img($x % 10);
      echo "<tr><td>$img</td><td>$name</td>";
      foreach ($ids as $id)
	{
	  $n = ($vals[$i] ? $vals[$i][$id+2] : 0);
	  echo "<td align=\"right\">$n</td>";
	}
      $total = $vals[$i][2] ? $vals[$i][2] : 0;
      echo "<td align=\"right\">$total</td>";
      echo '</tr>';
      $x += 1;
    }

  echo "</tr>";
  echo '</table>';
}

$meses = array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre");
sscanf($_GET['fecha_inicio'], "%d-%d-%d", $y, $m, $d);
$mes = $meses[$m];
$fecha_inicio = "$d de $mes del $y";
sscanf($_GET['fecha_fin'], "%d-%d-%d", $y, $m, $d);
$mes = $meses[$m];
$fecha_final = "$d de $mes del $y";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head><title>Informe</title></head>
  <body>
    <table cellpadding="10" cellspacing="10" align="center">
      <tr>
	<td>
	  <center><img src="logo_pdm.jpg" /></center>
	</td>
	<td>
	  <center><img src="logo_care.jpg" /></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php echo "Desde $fecha_inicio hasta el $fecha_final"; ?></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
  <center><h4>INFORME DE RESULTADOS Y ESTAD&Iacute;STICA DEL CENTRO DE AZCAPOTZALCO DE RESPUESTA A EMERGENCIA</h4></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php print_medios_de_recepcion_graph($cmedios_result); ?></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php print_medios_de_recepcion_table($cmedios_result, $ctotal); ?></center>
	</td>
      </tr>
<!-- PAGE BREAK -->
      <tr>
	<td>
	  <center><img src="logo_pdm.jpg" /></center>
	</td>
	<td>
	  <center><img src="logo_care.jpg" /></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php print_canalizaciones_graph($ccanalizaciones_result); ?></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php print_canalizaciones_table($ccanalizaciones_result, $ctotal); ?></center>
	</td>
      </tr>
<!-- PAGE BREAK -->
      <tr>
	<td>
	  <center><img src="logo_pdm.jpg" /></center>
	</td>
	<td>
	  <center><img src="logo_care.jpg" /></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php print_sucesos_graph($csucesos_result); ?></center>
	</td>
      </tr>
      <tr>
	<td colspan="2">
	  <center><?php print_sucesos_table($cmedios_por_suceso_result); ?></center>
	</td>
      </tr>
    </table>
  </body>
</html>
<?php

if ($con)
  end_connection($con);
?>
