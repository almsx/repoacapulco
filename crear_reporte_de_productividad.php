<?php
$allow = array(1 => true, 2 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="reportes.php">Reportes</a>: 
      </span>
      <span id="title_center">Crear Reporte de productividad</span>
    </div>
    <form target="_blank" action="ver_reporte_de_productividad.php" method="GET">
      <div id="form">
      <label class="frm" for="fecha_inicio">Fecha inicial:</label>
      <script>DateInput('fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="fecha_fin">Fecha final:</label>
      <script>DateInput('fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="users[]">Usuarios:</label>
      <select class="frm" id="users_sel" name="users[]" multiple="true" size="10">
      <?php make_usersoptions("users", true); ?>
      </select>
      <br />
      <label class="frm" for="dependencia">Dependencia:</label>
      <?php make_input('dependencia'); ?>
      <label class="frm" for="turno">Turno:</label>
      <select class="frm" id="turno" name="turno">
	<?php
           make_option("turno", "-", "-1");
	   make_option("turno", "Matutino", "0"); 
	   make_option("turno", "Vespertino", "1"); 
	   make_option("turno", "Nocturno", "2"); 
	?>
      </select>
      <br />
      <input id="ver" name="ver" type="submit" value="Ver" />
      <br />
      </div>
    </form>
<?php
include("footer.html");

end_connection($con);
?>
