<?php
$allow = array(1 => true);

include('session.inc');
include('utils.inc');

$con = make_connection();

if (1 == $_POST["commit"])
  { 
    $id = sqlquote($_POST['camara']);
    $query = "UPDATE camaras SET borrado = 1 WHERE id = $id;";

    mysql_query($query);
  }

$redirect = $_POST['borrar'];
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=admin_camaras.php" />';
  }

include("header.html");

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="admin.php">Administrador</a>: 
        <a class="title_link" href="admin_camaras.php">C&aacute;maras</a>: 
      </span>
      <span id="title_center">Borrar</span>
    </div>
    <form action="borrar_camara.php" method="POST">
      <div id="form">
        <input type="hidden" style="display: none;" name="commit" value="1" />
	<label class="frm" for="camara">C&aacute;mara:</label>
	<?php make_select('camara', 'camaras'); ?>
	<br />
	<br />
	<br />
	<input id="borrar" name="borrar" type="submit" value="Borrar" />
	<br />
      </div>
    </form>

<?php
  }
include("footer.html");
end_connection($con);
?>
