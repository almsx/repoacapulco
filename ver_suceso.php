<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

include('session.inc');
include('utils.inc');

$noexit = true;
$print = true;

include('header.html');

$result = NULL;
$con = NULL;

$perfiles = array(1 => "Administrador", "Supervisor", "Operador");

$id = ($_GET['id'] ? $_GET['id'] : $_POST['id']);

if ($id)
  {
    $con = make_connection();

    if ($_POST['id'])
      {
	$cquery = 'UPDATE sucesos SET abierto = ' . $_POST['estado'] .
	  ', tipo_de_registro_id = ' . $_POST['tipo_registro'];
	
	if ($_POST['estado'] == "0")
	  {
	    $tbp = "TIMEDIFF(NOW(), ADDTIME(fecha, hora))";
	    $cquery .= ', tiempo_base_plata = ' . $tbp . ', ' .
	      'usuario_cierra = ' . sqlquote($_SESSION['login_name']) . ', ' .
	      'perfil_usuario_cierra = ' . sqlquote($perfiles[$_SESSION['profile']]) . ", " .
	      'notas_cierra = ' . sqlquote($_POST['notas_cierra']);
	  }

	$cquery .= ' WHERE id = ' . $id . ';';

        mysql_query($cquery);
      }

    $query = 'SELECT id, fecha, hora, medio_de_recepcion_id, tipo_de_suceso_id, canalizacion_id, proteccion_civil, seguridad_publica, turno, tiempo_care, tiempo_base_plata, notas_abre, suceso_relevante, tipo_de_registro_id, abierto, usuario_crea, perfil_usuario_crea, usuario_cierra, perfil_usuario_cierra, notas_cierra FROM sucesos WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }
?>
<div id="title"><span id="title_center">Ver suceso</span></div>
<?php

  $row = mysql_fetch_row($result);

if ($row)
  {
    $medios_de_recepcion = get_poplist_table_array("medios_de_recepcion");
    $tipos_de_suceso = get_poplist_table_array("tipos_de_suceso");
    $tipos_de_registro = array("Verídico", "Accidental");
    $turnos = array("Matutino", "Vespertino", "Nocturno");
    $canalizaciones = get_poplist_table_array("canalizaciones");
    $suceso_relevante = array("No", "Si");
    $estados = array("Cerrado", "Abierto");
    $v = array('id' => $row[0],
	       'fecha' => $row[1] . ' ' . $row[2],
	       'suceso' => $tipos_de_suceso[$row[4]],
	       'canalizacion' => $canalizaciones[$row[5]],
	       'proteccion_civil' => $row[6],
	       'seguridad_publica' => $row[7],
	       'turno' => $turnos[$row[8]],
	       'tiempo_care' => substr($row[9], 3, 5),
	       'tiempo_base_plata' => substr($row[10], 0, 5),
	       'notas_abre' => uhtmlentities($row[11]),
	       'suceso_relevante' => $suceso_relevante[$row[12]],
	       'tipo_registro' => $tipos_de_registro[$row[13]],
	       'estado' => $estados[$row[14]],
	       'usuario_crea' => $row[15] . ' (' . $row[16] . ')',
	       'usuario_cierra' => $row[17] . ' (' . $row[18] . ')',
	       'notas_cierra' => uhtmlentities($row[19]));
?>
    <form action="ver_suceso.php" method="POST">
    <div id="form">
    <input type="hidden" style="display: none;" name="id" value=<? echo '"' . $row[0] . '"'; ?> />
    <label class="frm" for="fecha">Fecha:</label>
    <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
           value=<? echo '"' . $row[1] . ' ' . $row[2] . '"'; ?> />
    <br />
    <?php

    $tipo = $row[3];

    switch ($tipo)
      {
      case 1:
        $squery = 'SELECT e.cuenta, e.cap_code, a.persona, a.colonia, a.domicilio, a.telefono FROM eventos_alarmas AS e LEFT JOIN alarmas AS a ON e.cuenta = a.id_alarma WHERE e.id = ' .
          $row[0];
        $sresults = mysql_query($squery);
        $srow = mysql_fetch_row($sresults);
      ?>
      <label class="frm" for="cuenta">ID de alarma:</label>
      <?php make_input('cuenta', array('cuenta' => $srow[0]), true); ?>
      <label class="frm" for="cap_code">Cap Code:</label>
      <?php make_input('cap_code', array('cap_code' => $srow[1]), true); ?>
      <br />
      <label class="frm" for="persona">Usuario de la alarma:</label>
      <?php make_input('persona', array('persona' => $srow[2]), true); ?>
      <label class="frm" for="colonia">Colonia:</label>
      <?php make_input('colonia', array('colonia' => $srow[3]), true); ?>
      <label class="frm" for="domicilio">Domicilio:</label>
      <?php make_input('domicilio', array('domicilio' => $srow[4]), true); ?>
      <!--<label class="frm" for="telefono">Tel&eacute;fono:</label>-->
      <?php //make_input('telefono', array('telefono' => $srow[5]), true);
	break;
      case 2:
      case 3:
        $sectores = get_poplist_table_array("sectores");
        $squery = 'SELECT direccion, colonia, sector_id, telefono, calles FROM eventos_radio_telefono WHERE id = ' .
           $row[0];
        $sresults = mysql_query($squery);
        $srow = mysql_fetch_row($sresults);
      ?>
      <label class="frm" for="direccion">Direcci&oacute;n:</label>
      <?php make_input('direccion', array('direccion' => $srow[0]), true); ?>
      <label class="frm" for="calles">Entre las calles:</label>
      <?php make_input('calles', array('calles' => $srow[4]), true); ?>
      <br />
      <label class="frm" for="colonia">Colonia:</label>
      <?php make_input('colonia', array('colonia' => $srow[1]), true); ?>
      <label class="frm" for="sector">Sector:</label>
      <?php make_input('sector', array('sector' => $sectores[$srow[2]]), true); ?>
      <br />
      <!--?php
	 if ($tipo == "2")
	   {
	     echo '<label class="frm" for="telefono">Tel&eacute;fono:</label>' . "\n";
	     make_input('telefono', array('telefono' => $srow[3]), true);
	   }
	break;
      case 4:
        $camaras = get_poplist_table_array("camaras");
        $squery = 'SELECT camara_id FROM eventos_monitores where id=' . $row[0];
        $sresults = mysql_query($squery);
        $srow = mysql_fetch_row($sresults);
      ?>-->
      <!--<label class="frm" for="camara">C&aacute;mara:</label>-->
      <?php //make_input('camara', array('camara' => $camaras[$srow[0]]), true);
	break;
      }
    echo '<br />' . "\n";
      ?>
      <label class="frm" for="suceso">Suceso:</label>
      <?php make_input('suceso', array('suceso' => $tipos_de_suceso[$row[4]]), true); ?>
      <br />
      <label class="frm" for="canalizacion">Canalizaci&oacute;n:</label>
      <?php make_input('canalizacion', $v, true); ?>
      <label class="frm" for="tiempo_care">Tiempo de Respuesta (MM:SS):</label>
      <?php make_input('tiempo_care', $v, true); ?>
      <br />
      <!--<label class="frm" for="proteccion_civil">Protecci&oacute;n Civil:</label>-->
      <?php //make_input('proteccion_civil', $v, true); ?>
      <!--<label class="frm" for="seguridad_publica">Seguridad P&uacute;blica:</label>-->
      <?php //make_input('seguridad_publica', $v, true); ?>
      <br />
      <label class="frm" for="turno">Turno:</label>
      <?php make_input('turno', $v, true); ?>
      <label class="frm" for="suceso_relevante">Suceso relevante:</label>
      <?php make_input('suceso_relevante', $v, true); ?>
      <label class="frm" for="notas">Notas al abrir:</label>
      <textarea class="frm" id="notas" readonly="true" name="notas">
      <?php //echo $v['notas_abre']; ?></textarea><br />
     <!--<label class="frm" for="notas">Notas al cerrar:</label>
      <textarea class="frm" name="notas_cierra" id="notas_cierra" <?php //if (!$row[14]) echo 'readonly="true"'; ?> name="notas"
	><?php //echo $v['notas_cierra']; ?></textarea><br />-->
      <label class="frm" for="tipo_registro">Tipo de registro:</label>
      <?php
	 if (!$row[14])
	   {
	     make_input('tipo_registro', $v, true);
           }
         else
           { ?>
      <select class="frm" id="tipo_registro" name="tipo_registro">
	<?php
	   $arr = array('tipo_registro' => $row[13]);
	   make_option("tipo_registro", "Ver&iacute;dico", "0", $arr);
	   make_option("tipo_registro", "Accidental", "1", $arr); 
	?>
      </select>
      <?php } ?>
      <label class="frm" for="estado">Estado:</label>
      <?php
	 if (!$row[14])
	   {
	     make_input('estado', $v, true);
           }
         else
           { ?>
      <select class="frm" id="estado" name="estado">
	<?php
	   $arr = array('estado' => $row[14]);
	   make_option("estado", "Abierto", "1", $arr);
	   make_option("estado", "Cerrado", "0", $arr); 
	?>
      </select><br />
      <?php
	   }
      ?>
      <label class="frm" for="usuario_crea">Usuario que captur&oacute;:</label>
      <?php make_input('usuario_crea', $v, true); ?>
      <?php
	 if ($row[14]) { ?>
      <br />
      <input id="enviar" name="enviar" type="submit" value="Aplicar" />
      <?php }
         else
	   { ?>
      <label class="frm" for="usuario_cierra">Usuario que cerr&oacute;:</label>
      <?php make_input('usuario_cierra', $v, true); ?>
      <br />
      <label class="frm" for="tiempo_base_plata">Tiempo base plata (HH:MM):</label>
      <?php make_input('tiempo_base_plata', $v, true); 
	   }?> 
      <br />
      </div>
      </form>
      <?php
  }
else
  {
    echo '<div class="mensaje">Registro no encontrado.</div>' . "\n";
  }

echo '</div>';

include("footer.html");

if ($con)
  end_connection($con);
?>
