<?php

function uhtmlentities($str)
{
  return htmlentities(utf8_decode($str));
}

function make_connection()
{
  include('config.inc');

  $con = mysql_connect($dbhost, $dbuser, $dbpass)
    or die('Fall&oacute; la conecci&oacute;n con el servidor de datos: ' . mysql_error());

  mysql_select_db($dbname, $con)
    or die('Error al seleccionar la base de datos: ' . mysql_error());

  return $con;
}

function end_connection($con)
{
  mysql_close($con);
}

function sqlquote($str)
{
  return "\"" . mysql_real_escape_string(trim(html_entity_decode($str))) . "\"";
}

function get_poplist_table_rows($table)
{
  $query = 'SELECT id, descripcion FROM ' . $table . ' WHERE borrado = 0 ORDER BY descripcion ASC;';
  $result = mysql_query($query)
    or die('Error al obtener la lista de ' . $table . ': ' .
	   mysql_error());
  
  return $result;
}

function get_poplist_table_array($table)
{
  $results = get_poplist_table_rows($table);
  $array = array();

  while ($row = mysql_fetch_row($results))
    $array[$row[0]] = $row[1];

  return $array;
}

function result_to_array($result)
{
  $a = array();
 
  while ($row = mysql_fetch_row($result))
    $array[] = $row;

  return $array;
}
  

function make_input($name, $vals = false, $readonly = false)
{
  if (!$vals)
    $vals = $_POST;

  echo '<input class="frm" type="text" id="' . $name . '" name="' . $name . '" ';

  if ($readonly)
    echo 'readonly="true" ';

  echo 'value="' . uhtmlentities($vals[$name]) . '" />';
}

function make_option($name, $text, $value, $vals = false)
{
  if (!$vals)
    $vals = $_POST;

  echo '<option value="' . $value . '" ';
  if ($vals[$name] == $value)
    echo 'selected="true"';
  echo '>' . $text . '</option>' . "\n";
}

function make_select($name, $table, $vals = false, $any = false)
{
  if (!$vals)
    $vals = $_POST;

  echo '<select class="frm" id="' . $name . '" name="' . $name . '">';

  $rows = get_poplist_table_rows($table);

  if ($any)
    make_option($name, "-", "-1", $vals);

  while ($row = mysql_fetch_row($rows))
    {
      make_option($name, uhtmlentities($row[1]), $row[0], $vals);
    }

  echo '</select>';
}

function make_usersoptions($name, $realname = false)
{
  if ($realname)
    $results = mysql_query("SELECT CONCAT(nombre, \" - \", nombre_persona, \" \", apellidos) FROM usuarios;");
  else
    $results = mysql_query("SELECT nombre FROM usuarios;");

  $i = 0;

  while ($row = mysql_fetch_row($results))
    {
      echo '<option value="' . $row[0] . '" ' . 'selected="true"' . '>' .
	$row[0] . '</option>' . "\n";
      $i += 1;
    }
}


function make_number_select($name, $min, $max, $default = "none")
{
  echo '<select class="numselect" id="' . $name . '" name = "' . $name . '">';
  
  for ($i = $min; $i <= $max; $i++)
    {
      $num = sprintf("%02d", $i);
      make_option($name, $num, $num, array($name => $default));
    }

  echo '</select>';
}

function make_pdf($filename, $options = "", $multi = false)
{
  include('config.inc');
  header("Content-Type: application/pdf");
  header("Content-Disposition: inline; filename=\"reporte-care.pdf\"");
  flush();

  $cmd = "ulimit -m 16384; htmldoc --no-localfiles --no-compression -t pdf14 --quiet --jpeg --webpage --header . . --footer . . --size letter --left 0.5in $options ";

  if (!$multi)
    $cmd .= "'$localurl/$filename'";
  else
    {
      foreach ($filename as $url)
	$cmd .= " '$localurl/$url'";
    }

  passthru($cmd);
}

?>
