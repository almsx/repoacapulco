<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);

include('session.inc');
include('utils.inc');

$noexit = true;
$print = true;

include('header.html');

$result = NULL;
$con = NULL;

$id = $_GET['id'];

if ($id)
  {
    $con = make_connection();

    $query = 'SELECT id, fecha, hora, nombre, apellido, direccion, telefono, id_alarma, numero_de_beeper, lugar_de_entrega, nombre_entrega FROM entrega_de_beepers WHERE id = ' . $id . ';';

    $result = mysql_query($query);
  }
?>
<div id="title"><span id="title_center">Ver entrega de beeper</span></div>
<?php

if ($result)
  {
    $row = mysql_fetch_row($result);
    $v = array('id' => $row[0],
	       'fecha' => $row[1] . ' ' . $row[2],
	       'nombre' => $row[3] . ' ' . $row[4],
	       'direccion' => $row[5],
	       'telefono' => $row[6],
	       'cuenta' => $row[7],
	       'numero' => $row[8],
	       'lugar_entrega' => $row[9],
	       'nombre_entrega' => $row[10]);

?>
    <form action="ver_entrega_de_beeper.php" method="POST">
    <div id="form">
    <input type="hidden" style="display: none;" name="id" value=<? echo '"' . $row[0] . '"'; ?> />
    <label class="frm" for="fecha">Fecha:</label>
    <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
           value=<? echo '"' . $row[1] . ' ' . $row[2] . '"'; ?> />
    <br />
    <label class="frm" for="nombre">Nombre:</label>
    <?php make_input('nombre', $v, true); ?>
    <label class="frm" for="direccion">Direcci&oacute;n:</label>
    <?php make_input('direccion', $v, true); ?>
    <br />
    <label class="frm" for="telefono">Tel&eacute;fono:</label>
    <?php make_input('telefono', $v, true); ?>
    <br />
    <label class="frm" for="cuenta">ID de alarma:</label>
    <?php make_input('cuenta', $v, true); ?>
    <label class="frm" for="numero">N&uacute;mero de beeper</label>
    <?php make_input('numero', $v, true); ?>
    <br />
    <label class="frm" for="nombre_entrega">Quien hizo la entrega:</label>
    <?php make_input('nombre_entrega', $v, true); ?>
    <label class="frm" for="lugar_entrega">Lugar de la entrega:</label>
    <?php make_input('lugar_entrega', $v, true); ?>
    <br />
    <?php
  }
else
  {
    echo '<div class="mensaje">Registro no encontrado.</div>' . "\n";
  }

echo '</div>';

include("footer.html");

if ($con)
  end_connection($con);
?>
