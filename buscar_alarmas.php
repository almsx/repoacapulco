<?php
$allow = array(1 => true, 2 => true, 3 => true, 4 => true);


if (!$_GET['pdf'])
  include('session.inc');
include("utils.inc");

$con = make_connection();

$results = NULL;

if ($_GET['buscar'])
  {
    $filters = array();
    $filter_string = "";
    $query = NULL;

    if ($_GET['ingreso_fecha_inicio'] && $_GET['ingreso_fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha_de_ingreso >= ' . sqlquote($_GET['ingreso_fecha_inicio']);
    if ($_GET['ingreso_fecha_fin'] && $_GET['ingreso_fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha_de_ingreso <= ' . sqlquote($_GET['ingreso_fecha_fin']);
    if ($_GET['solicitud_fecha_inicio'] && $_GET['solicitud_fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha_de_solicitud >= ' . sqlquote($_GET['solicitud_fecha_inicio']);
    if ($_GET['solicitud_fecha_fin'] && $_GET['solicitud_fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha_de_solicitud <= ' . sqlquote($_GET['solicitud_fecha_fin']);
    if ($_GET['entrega_fecha_inicio'] && $_GET['entrega_fecha_inicio'] != "2006-01-01")
      $filters[] = 'fecha_de_entrega >= ' . sqlquote($_GET['entrega_fecha_inicio']);
    if ($_GET['entrega_fecha_fin'] && $_GET['entrega_fecha_fin'] != date("Y-m-d"))
      $filters[] = 'fecha_de_entrega <= ' . sqlquote($_GET['entrega_fecha_fin']);
    if (0 < strlen(trim($_GET['folio'])))
      $filters[] = 'folio LIKE ' . sqlquote('%' . strtoupper($_GET['folio']) . '%');
    if ($_GET['estado'] && $_GET['estado'] != "-1")
      $filters[] = 'entregada = ' . $_GET['estado'];

    if (count($filters) > 0)
      $filterstr .= "WHERE " . implode(" AND ", $filters);

    $query = "SELECT id, fecha_de_ingreso, fecha_de_solicitud, fecha_de_entrega, folio, entregada FROM solicitud_de_alarmas $filterstr;";

    $results = mysql_query($query);
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include("header.html");

?>
    <?php if (!$_GET['pdf']) { ?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="alarmas.php">Solicitud de alarmas</a>:
      </span>
      <span id="title_center">B&uacute;squeda</span>
    </div>
    <form action="buscar_alarmas.php" method="GET">
      <div id="form">
      <label class="frm" for="ingreso_fecha_inicio">Fecha de ingreso inicial:</label>
      <script>DateInput('ingreso_fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="ingreso_fecha_fin">Fecha de ingreso final:</label>
      <script>DateInput('ingreso_fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="solicitud_fecha_inicio">Fecha de solicitud inicial:</label>
      <script>DateInput('solicitud_fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="solicitud_fecha_fin">Fecha de solicitud final:</label>
      <script>DateInput('solicitud_fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="entrega_fecha_inicio">Fecha de entrega inicial:</label>
      <script>DateInput('entrega_fecha_inicio', true, 'YYYY-MM-DD', '2006-01-01')</script>
      <label class="frm" for="entrega_fecha_fin">Fecha de entrega final:</label>
      <script>DateInput('entrega_fecha_fin', true, 'YYYY-MM-DD')</script>
      <br />
      <label class="frm" for="folio">Folio:</label>
      <?php make_input('folio', $_GET); ?>
      <label class="frm" for="estado">Estado:</label>
      <select class="frm" id="estado" name="estado">
	<?php
	   make_option("estado", "-", "-1", $_GET);
	   make_option("estado", "Entregada", "true", $_GET);
	   make_option("estado", "Por entregar", "false", $_GET);
	?>
      </select><br />
      <br />
      <input id="buscar" name="buscar" type="submit" value="Buscar" />
      <br />
      </div>
    </form>
    <?php
      if ($_GET['buscar'])
	{
	  $url = "buscar_alarmas.php?" . urlencode(http_build_query($_GET));
	  echo "<a target=\"_blank\" href=\"make_pdf.php?url=$url\">Exportar a PDF</a>";
	}
      }
    ?>
    <?php if ($_GET['pdf']) { echo '<center><h3>Solicitudes de alarma</h3></center>' . "\n"; } ?>
    <hr />
<?php

if ($results)
  {
    $headers = array("ID", "Fecha de ingreso", "Fecha de solicitud", "Fecha de entrega", "Folio", "Estado");

    echo '<div id="search_results">' . "\n";
    echo '<table id="search_results">' . "\n";
    echo '<tr class="header">';
    
    foreach ($headers as $hd)
      echo '<th class="header">' . $hd . '</th>';

    $num = 0;

    while ($row = mysql_fetch_row($results))
      {
	echo '<tr onClick="window.open(\'ver_alarma.php?id=' .
	  $row[0] . '\', \'Suceso\', \'width=730,height=500,status=0,toolbar=0\');" onmouseover="this.style.backgroundColor = \'#ffffff\';" onmouseout="this.style.backgroundColor = \'#f5f1e8\';" class="result">';

	echo '<td class="result_cell">' . $row[0] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[1] . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[2] . '</td>' . "\n";
	echo '<td class="result_cell">' . ($row[3] == "0000-00-00" ? "-" : $row[3]) . '</td>' . "\n";
	echo '<td class="result_cell">' . $row[4] . '</td>' . "\n";
	echo '<td class="result_cell">' . ($row[5] ? "Entregada" : "Por entregar") . '</td>' . "\n";
	echo '</tr>' . "\n";
	$num += 1;
      }
    echo '</table>' . "\n";
    echo "<br /><p>Total de registros: $num</p>";
    echo '</div>' . "\n";
  }

include("footer.html");

end_connection($con);
?>
