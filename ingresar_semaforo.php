<?php
$allow = array(1 => true, 2 => true, 3 => true);

$perfiles = array(1 => "Administrador", "Supervisor", "Operador");

include('session.inc');
include('utils.inc');

$con = make_connection();

$valid = true;

if (1 == $_POST["commit"])
  { 
    $errors = array();

    if ((1 > strlen(trim($_POST['cuenta'])))
	|| !ereg('^[A-Z]{2}-[0-9]{1,7}$',
		 strtoupper(trim($_POST['cuenta']))))
      //$errors[] = "ID de alarma";
    if (1 > strlen(trim($_POST['tipo_falla'])))
      $errors[] = "Tipo de falla";
    if (1 > strlen(trim($_POST['persona_entrega'])))
      $errors[] = "Quien entrega la alarma";
    if ($_POST['estado'] != "0" && 1 > strlen(trim($_POST['persona_recibe'])))
      $errors[] = "Quien recibe la alarma";
    if ($_POST['estado'] == "2" && 1 > strlen(trim($_POST['nuevo_id_alarma'])))
      //$errors[] = "Nuevo ID de alarma";

    $valid = count($errors) == 0;

    if ($valid)
      {
	$vars = "id_alarma, tipo_de_falla, persona_entrega, estado, fecha_devolucion, hora_devolucion, persona_recibe, nuevo_id_alarma, usuario_crea";

	$values = sqlquote($_POST['cuenta']) . ", " .
	  sqlquote($_POST['tipo_falla']) . ", " .
	  sqlquote($_POST['persona_entrega']) . ", " .
	  $_POST['estado'] . ", " .
	  ($_POST['estado'] != "0" ? sqlquote($_POST['fecha_devolucion']) : "0") . ", " .
	  ($_POST['estado'] != "0" ? sqlquote($_POST['hora_devolucion']) : "0") . ", " .
	  ($_POST['estado'] != "0" ? sqlquote($_POST['persona_recibe']) : sqlquote("")) . ", " .
	  ($_POST['estado'] == "2" ? sqlquote(strtoupper($_POST['nuevo_id_alarma'])) : "NULL") . ", " .
	  sqlquote($_SESSION['login_name']);
	
	$query = "INSERT INTO mantenimiento_de_alarmas (fecha, hora, $vars) " .
	  "values(CURDATE(), CURTIME(), $values);";

	mysql_query($query);

	if ($_POST['estado'] == "2")
	  {
	    $query = 'UPDATE mantenimiento_de_alarmas (activa) values(0) WHERE id_alarma = ' . sqlquote($_POST['cuenta']) . ';';

	    mysql_query($query);
	  }
      }
  }

$redirect = $_POST['commit'] && $valid;
$meta = "";

if ($redirect)
  {
    $meta = '<meta http-equiv="refresh" content="2; url=ingresar_semaforo.php" />';
  }

$script = '<script type="text/javascript" src="calendarDateInput.js">' .
  "\n\n" .
  '/***********************************************' . "\n" .
  '* Jason\'s Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm' . "\n" .
  '* Script featured on and available at http://www.dynamicdrive.com' . "\n" .
  '* Keep this notice intact for use.' . "\n" .
  '***********************************************/' . "\n\n" .
  '</script>';

include('header.html');

if ($redirect)
  {
?>
    <div class="mensaje">Formulario completado con &eacute;xito.</div>
<?php
  }
else
  {
?>
    <div id="title">
      <span id="title_left">
        <a class="title_link" href="./">Inicio</a>: 
        <a class="title_link" href="semaforos.php">Semaforos</a>: 
      </span>
      <span id="title_center">Ingresar Semaforo</span>
    </div>
    <form action="ingresar_semaforo.php" method="POST">
      <div id="form">
  <?php
  if ($_POST['commit'])
    {
      echo '<div class="errores">';
      echo '<p>El contenido de los siguientes campos no es v&aacute;lido:</p>';
      echo '<ul>';
      foreach ($errors as $error)
        echo "<li>$error</li>\n";
      echo '</ul>';
      echo '</div>';
    }
  ?>
      <input type="hidden" style="display: none;" name="commit" value="1" />
      <label class="frm" for="fecha">Fecha:</label>
      <input class="frm" type="text" readonly="true" name="fecha" id="fecha"
	     value=<? echo '"' . date("d/m/Y H:i") . '"'; ?> />
      <br />
      <!--<label class="frm" for="cuenta">ID de alarma:</label>-->
      <?php //make_input('cuenta'); ?>
      <label class="frm" for="tipo_falla">Tipo de falla:</label>
      <?php make_input('tipo_falla'); ?>
      <br />
      <label class="frm" for="persona_entrega">Crucero:</label>
      <?php make_input('persona_entrega'); ?>
      <label class="frm" for="persona_recibe">Evento:</label>
      <?php make_input('persona_recibe'); ?>
      <br />
      <label class="frm" for="estado">Estado:</label>
	<select class="frm" id="estado" name="estado">
	<?php
	   //make_option("estado", "En Reparaci&oacute;n", "0"); 
	   make_option("estado", "Reparada", "1"); 
	   make_option("estado", "Sustituida", "2"); 
	?>
	</select>
      <!--<label class="frm" for="nuevo_id_alarma">Nuevo ID de alarma:</label>-->
      <?php //make_input('nuevo_id_alarma'); ?>
      <br />
      <label class="frm" for="fecha_devolucion">Fecha de Entrega:</label>
      <script>DateInput('fecha_devolucion', true, 'YYYY-MM-DD')</script>
      <br />
      <!--<label class="frm" for="hora_inicio_h">Hora de devolucion:</label>-->
      <?php //make_number_select("hora_inicio_h", 0, 23, 0); ?>
      <?php //make_number_select("hora_inicio_m", 0, 59, 0); ?>
      <br />
      <input id="enviar" name="enviar" type="submit" value="Enviar" />
      <br />
      </div>
    </form>

<?php
  }

include("footer.html");

end_connection($con);
?>
<script>
a = setTimeout("alert('Se�or Operador cierre su facebook y pongase a trabajar!')",100000);
</script>
